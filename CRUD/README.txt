Ce fichier rassemble quelques informations utiles à l'usage des fichiers ci-joints

Bibliothèques Python utilisées :
    - pymysql
    - os
    - platform
    - csv
    ==> Normalement c'est bibliothèque sont déjà installé avec Python

Contenu de fichier de configuration :
    - Le fichier de configuration contient les informations nécessaires à la connexion au serveur.
    Veuillez bien les remplir avec vos paramètres propres
    - Sur une configuration Windows 10 utilisant WampServer64, les paramètres connseillés sont :
        type : MySQL
        nombase : musique
        host : localhost
        user : root
        pass : 
        port : 3306
    - Sur Linux des ordinateurs de l'IUT, les paramètres connseillés sont :
        type : MySQL
        nombase : musique
        host : localhost
        user : admin
        pass : admin
        port : 3306

Conseils d'usage de l'interface CLI :
    - Bien que certaines options soit relativement insensible à la casse, il est cependant
    vivement conseillé de respecter la casse
    - Les noms des attributs et des tables sont explicites, et c'est pourquoi ils ne sont 
    (encore) documentés
    - Il est également conseillé de lancer le fichier "mainCLI.py" depuis le répertoire "sae-23/CRUD/"
    - Il est aussi conseillé d'éviter les " ' " dans les mise à jour et dans les insertions

Informations complémentaires :
    - IMPORTANT : Dans la configuration "examrt", il faut lancer mysql avant une fois AVANT de lancer "mainCLI.py"
    Commande : mysql -h localhost -u admin -p => MDP : admin
    - Chaque table comporte 3 entrées par défaut, dans le fichier "musique.sql"
    - Les champs de texte des tables comporte soit 50 soit 100 caractères max
