# Axel MOURILLON - SAE 2.3 

_requetes = {
    # --- Create : requêtes de création de la base et des tables, et insertions de valeurs de bases
    # Nettoyage et usage de la base de données
    "drop" : "DROP DATABASE IF EXISTS musique ;",
    "createBase" : "CREATE DATABASE IF NOT EXISTS musique DEFAULT CHARACTER SET utf8 ;",
    "use" : "USE musique",
    # Création des tables
    "createTableAlbum" : "CREATE TABLE album ( \
        numAlbum INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomAlbum VARCHAR(50) NOT NULL,\
        classication ENUM('Single','EP','Album') NOT NULL,\
        dateSortie DATE NOT NULL) ENGINE=InnoDB ;",
    "createTableArtiste" :"CREATE TABLE artiste ( \
        numArtiste INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomArtiste VARCHAR(50) NOT NULL,\
        descripArt VARCHAR(100) NOT NULL) ENGINE=InnoDB ;",
    "createTableTitre" :"CREATE TABLE titre ( \
        numTitre INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomTitre VARCHAR(50) NOT NULL,\
        duree TIME NOT NULL, \
        numAlbum INTEGER NOT NULL, \
        numArtiste INTEGER NOT NULL) ENGINE=InnoDB ;",
    "createTablePlaylist" :"CREATE TABLE playlist ( \
        numPlaylist INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomPlaylist VARCHAR(50) NOT NULL,\
        theme VARCHAR(100) NOT NULL,\
        dateCreation DATE NOT NULL DEFAULT (CURRENT_DATE), \
        numCreateur INTEGER NOT NULL) ENGINE=InnoDB ;",
    "createTableUtilisateur" :"CREATE TABLE utilisateur (\
        numUtil INTEGER PRIMARY KEY AUTO_INCREMENT,\
        pseudonyme VARCHAR(50) NOT NULL,\
        descripUtil VARCHAR(100) NOT NULL) ENGINE=InnoDB ;",
    "createTableRassembler" :"CREATE TABLE rassembler ( \
        numInteraTiPl INTEGER PRIMARY KEY AUTO_INCREMENT, \
        numTitre INTEGER NOT NULL,\
        numPlaylist INTEGER NOT NULL) ENGINE=InnoDB ;",
    "createTablePosseder" :"CREATE TABLE posseder ( \
        numInteraAlAr INTEGER PRIMARY KEY AUTO_INCREMENT, \
        numAlbum INTEGER NOT NULL,\
        numArtiste INTEGER NOT NULL) ENGINE=InnoDB ;  ",
    # Création des relations
    "createRelationGrouper" : "ALTER TABLE titre ADD CONSTRAINT grouper \
        FOREIGN KEY (numAlbum) \
        REFERENCES album(numAlbum) ON DELETE CASCADE ON UPDATE CASCADE ;",
    "createRelationComposer" : "ALTER TABLE titre ADD CONSTRAINT composer \
        FOREIGN KEY (numArtiste) \
        REFERENCES artiste(numArtiste) ON DELETE CASCADE ON UPDATE CASCADE ;",
    "createRelationPossederSideAl" : "ALTER TABLE posseder ADD CONSTRAINT possederSideAl \
        FOREIGN KEY (numAlbum) \
        REFERENCES album(numAlbum) ON DELETE CASCADE ON UPDATE CASCADE ;",
    "createRelationPossederSideAr" : "ALTER TABLE posseder ADD CONSTRAINT possederSideAr \
        FOREIGN KEY (numArtiste) \
        REFERENCES artiste(numArtiste) ON DELETE CASCADE ON UPDATE CASCADE ;",
    "createRelationRassemblerSideT" : "ALTER TABLE rassembler ADD CONSTRAINT rassemblerSideT \
        FOREIGN KEY (numTitre) \
        REFERENCES titre(numTitre) ON DELETE CASCADE ON UPDATE CASCADE ;",
    "createRelationRassemblerSideP" : "ALTER TABLE rassembler ADD CONSTRAINT rassemblerSideP \
        FOREIGN KEY (numPlaylist) \
        REFERENCES playlist(numPlaylist) ON DELETE CASCADE ON UPDATE CASCADE ;",
    "createRelationConstituer" : "ALTER TABLE playlist ADD CONSTRAINT constituer \
        FOREIGN KEY (numCreateur) \
        REFERENCES utilisateur(numUtil) ON DELETE CASCADE ON UPDATE NO ACTION ;",
    # Insertion de valeurs par défauts - INUTILE pour le moment
    "defaultInsertionsArtiste" : "INSERT INTO artiste (nomArtiste,descripArt) VALUES \
        ('NF','Rappeur américain'),\
        ('Danger','DJ Français'),\
        ('Au/Ra','Pop US');",
    "defaultInsertionsAlbum" : "INSERT INTO album (nomAlbum,classication,dateSortie) VALUES \
        ('CLOUDS','Album','2021-02-18'),\
        ('Origins','Album','2019-01-18'),\
        ('X Games','EP','2018-10-19');",
    "defaultInsertionsTitre" : "INSERT INTO titre (nomTitre,duree,numAlbum,numArtiste) VALUES \
        ('CLOUDS','00:04:03',1,1),\
        ('22:39','00:04:12',2,2),\
        ('Panic Room','00:03:58',3,3);",
    "defaultInsertionsUtilisateur" : "INSERT INTO utilisateur (pseudonyme,descripUtil) VALUES \
        ('PulseStar5k','THE Admin, aime beaucoup de styles différents (mais surtout l indé)'),\
        ('Triston','C est Tristan, il aime le thon et la phonk'),\
        ('Il_Duccini','TheBestProfDInfo');",
    "defaultInsertionsPlaylist" : "INSERT INTO playlist (nomPlaylist,theme,numCreateur) VALUES \
        ('US Rap','Rap US uniquement',1),\
        ('Indé','Musiques indépendantes sympa',1),\
        ('Gonna Hit The Bench','Boom Boom pour la muscu',2);",
    "defaultInsertionsRassembler" : "INSERT INTO rassembler (numTitre,numPlaylist) VALUES \
        (1,3),\
        (2,3),\
        (3,2);",
    "defaultInsertionsPosseder" : "INSERT INTO posseder (numAlbum,numArtiste) VALUES \
        (1,1),\
        (2,2),\
        (3,3);",
        
    # Insertion par tables
    "insert" : """INSERT INTO {} ({}) VALUES ({});""",
    
    # --- Read : requêtes de lecture de la base
    "showTables" : "SHOW TABLES ;",
    "get" : "SELECT {} FROM {} ;",
    "getAll" : "SELECT * FROM {} ;",
    
    # --- Update : requête de mise à jour de la base
    "update" : """UPDATE {} SET {} = '{}' WHERE {} = '{}'""",
    
    # --- Delete : requêtes de suppression dans la base
    "delete" : "delete from {} where {} = {};" ,
    "resetTable" : "delete from {};",

    # --- CSV : requêtes nécessaire à l'extraction des données depuis un fichier CSV
    "insertArtiste" : "INSERT INTO artiste (nomArtiste,descripArt) VALUES ('{}', '{}');",
    "insertAlbum" : "INSERT INTO album (nomAlbum,classication,dateSortie) VALUES ('{}', '{}', '{}');",
    "insertTitre" : "INSERT INTO titre (nomTitre,duree,numAlbum,numArtiste) VALUES ('{}', '{}', {}, {});",
    "insertUtilisateur" : "INSERT INTO utilisateur (pseudonyme,descripUtil) VALUES ('{}', '{}');",
    "insertPlaylist" : "INSERT INTO playlist (nomPlaylist,theme,dateCreation,numCreateur) VALUES ('{}', '{}', '{}', {});",
    "insertRassembler" : "INSERT INTO rassembler (numTitre,numPlaylist) VALUES ({}, {});",
    "insertPosseder" : "INSERT INTO posseder (numAlbum,numArtiste) VALUES ({}, {});"
}

# Extraction de données à partir d'un fichier CSV
#   Routines renvoyant la requête d'insertion de données en fonction des attributs de leurs tables respectives
def insertArtiste(nomArtiste : str, descripArt : str) :
    return _requetes["insertArtiste"].format(nomArtiste, descripArt)
def insertAlbum(nomAlbum : str , classication : str, dateSortie : str) :
    return _requetes["insertAlbum"].format(nomAlbum, classication, dateSortie)
def insertTitre(nomTitre : str, duree : str, numAlbum : int, numArtiste : int) :
    return _requetes["insertTitre"].format(nomTitre, duree, numAlbum, numArtiste)
def insertUtilisateur(pseudonyme : str, descripUtil : str) :
    return _requetes["insertUtilisateur"].format(pseudonyme, descripUtil)
def insertPlaylist(nomPlaylist : str, theme : str, dateCreation : str, numCreateur : int) :
    return _requetes["insertPlaylist"].format(nomPlaylist, theme, dateCreation, numCreateur)
def insertRassembler(numTitre : int, numPlaylist : int) :
    return _requetes["insertRassembler"].format(numTitre, numPlaylist)
def insertPosseder(numAlbum : int, numArtiste : int) :
    return _requetes["insertPosseder"].format(numAlbum, numArtiste)