# Axel MOURILLON - SAE 2.3 

# Importations
import pymysql
from configDatabase import *
from SQLRequestRef import _requetes

def creationTables(cursor) :
    """ Rassemble les exécutions de requêtes de création de table

    Args:
        cursor : curseur (ou query)
    """
    cursor.execute(_requetes["createTableAlbum"])
    cursor.execute(_requetes["createTableArtiste"])
    cursor.execute(_requetes["createTableTitre"])
    cursor.execute(_requetes["createTablePlaylist"])
    cursor.execute(_requetes["createTableUtilisateur"])
    cursor.execute(_requetes["createTableRassembler"])
    cursor.execute(_requetes["createTablePosseder"])

def creationRelations(cursor) :
    """ Rassemble les exécutions de requêtes de création de relation

    Args:
        cursor : curseur (ou query)
    """
    cursor.execute(_requetes["createRelationGrouper"])
    cursor.execute(_requetes["createRelationComposer"])
    cursor.execute(_requetes["createRelationPossederSideAl"])
    cursor.execute(_requetes["createRelationPossederSideAr"])
    cursor.execute(_requetes["createRelationRassemblerSideT"])
    cursor.execute(_requetes["createRelationRassemblerSideP"])
    cursor.execute(_requetes["createRelationConstituer"])

def defautInsert(cursor) :
    """ Rassemble les exécutions de requêtes d'insertion de données prédéfinies dans la base

    Args:
        cursor : curseur (ou query)
    """
    cursor.execute(_requetes["defaultInsertionsArtiste"])
    cursor.execute(_requetes["defaultInsertionsAlbum"])
    cursor.execute(_requetes["defaultInsertionsTitre"])
    cursor.execute(_requetes["defaultInsertionsUtilisateur"])
    cursor.execute(_requetes["defaultInsertionsPlaylist"])
    cursor.execute(_requetes["defaultInsertionsRassembler"])
    cursor.execute(_requetes["defaultInsertionsPosseder"])

def createBaseMySQL() :
    """ Créer la base lorsque que celle-ci n'existe pas """
    # Connexion au serveur, sans chercher à se connecter à la base
    _db, _cursor = dbConnect(False)
    # Commande préalable, pour une création de table propre
    _cursor.execute(_requetes["drop"])
    _cursor.execute(_requetes["createBase"])
    _cursor.execute(_requetes["use"])
    # Créations des tables :
    creationTables(_cursor)
    # Créations des relations :
    creationRelations(_cursor)
    # Insertion de valeurs par défaut : présente pour tester la base, ou la créée avec des valeurs depuis python
    # defautInsert(_cursor)
    # Sauvegarde des commandes exécutées précédemment
    _db.commit()

def executeRequest(req : str) :
    """ Exécute une requête SQL, la sauvegarde, et renvoie son compte-rendu d'exécution et son résultat

    Args:
        req (str): requête à exécuter

    Returns:
        tuple: tuple rassemblant le compte-rendu d'exécution et le résultat de la requête
    """
    _db, _cursor = dbConnect(True)
    cr = _cursor.execute(req)
    res = _cursor.fetchall()
    _db.commit()
    return cr,res

def executeSQL() :
    """ Interface de test et de debug, pour l'exécution d'une requête SQL directement depuis le CLI sur la base """
    req = input("Entrez la requête à exécuter : ")
    try :
        cr, result = executeRequest(req)
        print("Compte-rendu d'exécution : ", cr)
        print("Résultat : ", result)
    except Exception as e :
        print("Erreur de la requête : ", e)

def tryConnect() : 
    """ Teste la connexion au serveur, et renvoie le résultat

    Returns:
        Le résultat est une erreur en cas d'échec de connexion, et la liste des tables tout s'est bien passé
    """
    # Tentative de connexion à la base de données
    _db, _cursor = dbConnect(True)
    # Tentative d'exécution d'une requête simple
    result = _cursor.execute(_requetes["showTables"])
    return result

def getTables() :
    """ Interroge la base de données et renvoie la liste des tables

    Returns:
        list: liste de chaîne de caractères, ressemblant les tables de la base
    """
    cr, result = executeRequest(_requetes["showTables"])
    listeTables = []
    for element in result :
        listeTables.append(element[0])
    return listeTables

def initBase() :
    """ Teste la connexion à la base sur le serveur hôte, et propose sa création si nécessaire

    Raises:
        e: erreur d'opération sur la base
        e: erreur de refus de construction de la base
    """
    try :
        tryConnect()
    except pymysql.err.OperationalError as e:
        if '1044' in str(e) :
            print ("Vérifiez les paramètres de connexion")
            raise e
        elif '1049' in str(e) :
            choix = input("La base 'musique' n'existe pas. Voulez-vouz créer cette base de données ? (Y/N) : ")
            if choix not in 'yYOo' : raise e
            else : createBaseMySQL()
