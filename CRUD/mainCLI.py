# Axel MOURILLON - SAE 2.3 

# Importations
from databaseUtils import initBase, getTables, executeRequest, executeSQL
from SQLRequestRef import _requetes, insertArtiste, insertAlbum, insertTitre, insertUtilisateur, insertPlaylist, insertRassembler, insertPosseder
import os, platform, csv

def affMenuAction(choixActions : list) :
    """ Affiche proprement les actions disponibles via l'interface CLI

    Args:
        choixActions (list): liste de tuple, contenant le label d'une action et sa routine associée
    """
    print()
    print("Actions disponibles :")
    for indexChoix in range(len(choixActions)):
        print(f'{indexChoix+1} : {choixActions[indexChoix][0]}')
    print(f'{len(choixActions)+1} : Quitter')
    print()

def askTable():
    """ Demande à l'utilisateur de renseigné une table parmi celle de la base de données.
    Effectue des vérifications et renvoie la chaîne de caractère correspondant au nom de 
    la table renseignée. Redemande tant que la réponse de l'utilisateur est erronée.

    Returns:
        str: table vérifié
    """
    ok = False
    retour = ""
    listeTable = getTables()
    while not(ok) :
        print("RAPPEL => ", end="")
        printTables()
        table = input("--> ")
        if table.upper() == "CANCEL" : 
            print("Retour au menu des actions ")
            retour = "0"
            ok = True
        elif table not in listeTable : print("Erreur, recommencez svp ")
        else : 
            retour = table.lower()
            ok = True
    return retour

def getAttributesTab(table) :
    """ Renvoie la liste des attributs de la table en paramètre

    Args:
        table (str): table dont on veut les attributs

    Returns:
        list: liste de chaîne de caractères
    """
    if table != "0" :
        if table.lower() == "artiste" : listeAttr = ["numArtiste", "nomArtiste", "descripArt"]
        elif table.lower() == "album" : listeAttr = ["numAlbum", "nomAlbum", "classication", "dateSortie"]
        elif table.lower() == "titre" : listeAttr = ["numTitre", "nomTitre", "duree", "numAlbum", "numArtiste"]
        elif table.lower() == "utilisateur" : listeAttr = ["numUtil", "pseudonyme", "descripUtil"]
        elif table.lower() == "playlist" : listeAttr = ["numPlaylist", "nomPlaylist", "theme", "dateCreation", "numCreateur"]
        elif table.lower() == "rassembler" : listeAttr = ["numInteraTiPl", "numTitre", "numPlaylist"]
        elif table.lower() == "posseder" : listeAttr = ["numInteraAlAr", "numAlbum", "numArtiste"]
        return listeAttr

def getNPrintAttrTab(table) :
    """ Récupère et affiche proprement les attributs de la table en paramètre

    Args:
        table (str): table dont on veut afficher et récupérer les attributs

    Returns:
        list: liste de chaîne de caractères, contenant les attributs de la table
    """
    attrTab = getAttributesTab(table)
    char = ""
    for index in range(len(attrTab)) :
        if index != len(attrTab) -1 : char += f"{attrTab[index]}, "
        else : char += f"{attrTab[index]} "
    print(f"Les attributs de cette tables sont : {char}")
    return attrTab

def verifyIfAttrAll(table) :
    """ Vérifie les attributs renseignés par l'utilisateur en fonction de la table
    à laquelle on s'intéresse. Gère également le cas où l'utilisateur veut tous les
    attributs. Renvoie les attributs renseignés après vérification

    Args:
        table (str): table dont on veut afficher et récupérer les attributs

    Returns:
        list: liste de chaîne de caractères, contenant les attributs de la table
    """
    attrTab = getNPrintAttrTab(table)
    attrSelect = input("--> ").split(",")
    good = True
    for element in attrSelect :
        if not(element in attrTab or element in "*") : good = False
    if good :
        attrReq = []
        if attrSelect[0] == "*" :
            for element in attrTab : 
                attrReq.append(element)
        else : attrReq = attrSelect
        return attrReq

def verifyYesNo(char : str) :
    """ Vérifie si la chaîne de caractère en paramètre répond par
    l'affirmative ou la affirmative (et ce correctement)

    Args:
        char (str): chaîne de caractère de réponse

    Returns:
        bool: True si affirmatif, False si affirmatif, et None si autre
    """
    if char.lower() == "yes" or char.lower() == "oui" : result = True
    elif char in "yYoO" : result = True
    elif char.lower() == "no" or char.lower() == "non" : result = False
    elif char in "nN" : result = False
    else : result = None
    return result
    
def printTables() :
    """ Affiche proprement les tables disponibles dans la base """
    listeTable = getTables()
    print("Tables de la base : ", end="")
    for indexTable in range(len(listeTable)) :
        if indexTable != len(listeTable)-1 : print(f"{listeTable[indexTable]}, ", end="")
        else : print(f"{listeTable[indexTable]}")

def getAttrInTab(attributs, table) :
    """ Effectue la requête de sélection dans la table renseignées en fonction des attributs choisis

    Args:
        attributs (list): liste de chaîne de caractères
        table (str): table dont on veut obtenir les attributs souhaités

    Returns:
        tuple: tuple de tuple rassemblant les résultats de la requête
    """
    insertAttrInReq = ""
    for index in range(len(attributs)) :
        insertAttrInReq += f"{attributs[index]}"
        if index != len(attributs)-1 : insertAttrInReq += ","
    cr, result = executeRequest(_requetes["get"].format(insertAttrInReq, table))
    return result

def resultToMatrix(result) :
    """ Transforme un tuple de tuple en liste de liste

    Args:
        result (tuple): tuple de tuple rassemblant les résultats de la requête

    Returns:
        list: liste de listes, ces-dernières contiennent les résultats
    """
    matrixResult = []
    for element in result :
        tempList = []
        for inElement in element :
            tempList.append(inElement)
        matrixResult.append(tempList)
    return matrixResult

def getAllFromTab(table):
    """ Raccourci la requête sélectionnant tous les attributs d'une table

    Args:
        table (str): table dont on veut afficher tous les attributs

    Returns:
        tuple: tuple de tuple rassemblant les résultats de la requête
    """
    cr, result = executeRequest(_requetes["getAll"].format(table))
    return result

def affMatrixResult(matrix, attributs) :
    """ Affiche proprement une liste de liste représentant les résultats d'une requête de sélection

    Args:
        matrix (list): liste de listes, ces-dernières contiennent les résultats
        attributs (list): liste de chaîne de caractères
    """
    charAttr = " "
    for index in range(len(attributs)) :
        charAttr += f"| {attributs[index]} "
    print(charAttr)
    for index in range(len(matrix)) : 
        charResult = " "
        for inElement in matrix[index] :
            charResult += f"| {inElement} "
        print(charResult)

def printValuesInTable(attributs, table, all) :
    """ Affiche proprement les valeurs d'une table en fonction des attributs en paramètre

    Args:
        attributs (list): liste de chaîne de caractères
        table (str): table dont on veut afficher les attributs
        all (bool): True si tous les attributs de la table sont sélectionnés, False sinon
    """
    if all : matrixResult = resultToMatrix(getAllFromTab(table))
    else : matrixResult = resultToMatrix(getAttrInTab(attributs, table))
    affMatrixResult(matrixResult, attributs)

def printAttributesTable() :
    """ Affiche proprement les valeurs d'une tables en fonction des attributs souhaités """
    print("Veuillez choisir la table, parmi celles ci-dessous, dont vous voulez afficher des valeurs. ")
    table = askTable()
    print("Veuillez choisir le(s) attributs, parmi ceux ci-dessous, que vous souhaitez insérer. Vous pouvez tous les sélectionnez avec : * ")
    print("ATTENTION : veuillez respecter la casse. Une erreur entrainera un retour au menu des actions. ")
    attributs = verifyIfAttrAll(table)
    if attributs == None :
        print("Erreur : retour au menu ")
    else :
        printValuesInTable(attributs, table, False)
        
def insertInTable():
    """ Insère les valeurs renseignés par l'utilisateur dans une table """
    print("Veuillez choisir la table, parmi celles ci-dessous, où vous souhaitez insérer des valeurs. ")
    table = askTable()
    attributs = getAttributesTab(table)
    attributsAInsert = ""
    for index in range(len(attributs)) :
        if index != 0 : 
            attributsAInsert += f"{attributs[index]}"
            if index != len(attributs)-1 : attributsAInsert += ","
    attributsAInsertList = attributsAInsert.split(",")
    print(f"Les attributs à insérer dans la table {table} sont les suivants : {attributsAInsert} ")
    print("Veuillez des valeurs en accord avec ces attributs. Respectez l'ordre !!! ")
    encore = True 
    while encore :
        char = ""
        for index in range(len(attributsAInsertList)) :
            valeur = input(f"Valeur pour l'attribut {attributsAInsertList[index]} --> ")
            char += f"'{valeur}'"
            if index != len(attributsAInsertList)-1 : char += ","
        req = _requetes["insert"].format(table, attributsAInsert, char)
        cr, result = executeRequest(req)
        continuer = input("Voulez-vous insérer plus de valeurs (Y/N) : ")
        encore = verifyYesNo(continuer)

def deleteInTable() :
    """ Supprime les valeurs renseignés par l'utilisateur dans une table """
    print("Veuillez choisir la table, parmi celles ci-dessous, où vous souhaitez supprimer des valeurs. ")
    table = askTable()
    attributsTab = getAttributesTab(table)
    print("Cette table contient les éléments suivants :")
    printValuesInTable(attributsTab, table, True)
    encore = True 
    while encore :
        charPrinted = "Veuillez choisir l'attribut, parmi ceux ci-dessous, qui vous aidera à sélectionner l'élément à supprimer. \n ATTENTION : veuillez respecter la casse. Une erreur entrainera un retour au menu des actions. "
        print(charPrinted)
        attribut = verifyIfAttrAll(table)
        attributADelete = attribut[0]
        print("Veuillez entrer l'élément à supprimer, conformément à votre choix d'attribut : ")
        valeur = input("--> ")
        req = _requetes["delete"].format(table, attributADelete, valeur)
        cr, result = executeRequest(req)
        print("Cette table contient désormais les éléments suivants :")
        printValuesInTable(attributsTab, table, True)
        continuer = input("Voulez-vous supprimer plus de valeurs (Y/N) : ")
        encore = verifyYesNo(continuer)
        print("---")

def deleteAllTable() :
    """ Insère toutes les valeurs d'une table """
    print("Veuillez choisir la table, parmi celles ci-dessous, dans laquelle vous souhaitez supprimer toutes valeurs. ")
    table = askTable()
    req = _requetes["resetTable"].format(table)
    cr, result = executeRequest(req)
    print("Table vidé de ses valeurs ")

def updateInTable() :
    """ Met à jour les valeurs renseignés par l'utilisateur dans une table """
    print("Veuillez choisir la table, parmi celles ci-dessous, où vous souhaitez mettre une valeur à jour. ")
    table = askTable()
    attributsTab = getAttributesTab(table)
    print("Cette table contient les éléments suivants :")
    printValuesInTable(attributsTab, table, True)
    encore = True 
    while encore :
        charPrinted = "Veuillez choisir l'attribut, parmi ceux ci-dessous, à mettre à jour. \n ATTENTION : veuillez respecter la casse. Une erreur entrainera un retour au menu des actions. "
        print(charPrinted)
        attribut = verifyIfAttrAll(table)
        attributAMaJ = attribut[0]
        print("Veuillez entrer la nouvelle valeur pour cet attribut : ")
        newValeur = input("--> ")
        charPrinted2 = "Veuillez maitenir choisir l'attribut qui vous servira à sélectionner l'élément à mettre à jour. \n ATTENTION : veuillez respecter la casse. Une erreur entrainera un retour au menu des actions. "
        print(charPrinted2)
        attribut = verifyIfAttrAll(table)
        attributSelect = attribut[0]
        print("Veuillez enfin entrer la valeur sélectionnant l'élément à mettre à jour : ")
        valeurSelect = input("--> ")
        req = _requetes["update"].format(table, attributAMaJ, newValeur, attributSelect, valeurSelect)
        cr, result = executeRequest(req)
        print("Cette table a bien été mise à jour :")
        printValuesInTable(attributsTab, table, True)
        continuer = input("Voulez-vous mettre à jour plus d'éléments (Y/N) : ")
        encore = verifyYesNo(continuer)
        print("---")

def saveInCSV():
    """ Extrait les données de toutes les tables dans des fichiers CSVs """
    for table in getTables() :
        cr,result = executeRequest(_requetes["getAll"].format(table))
        matrixResult = resultToMatrix(result)
        lignes = []
        for indexList in range(len(matrixResult)) :
            charTemp = ""
            for indexElem in range(len(matrixResult[indexList])) :
                charTemp += str(matrixResult[indexList][indexElem])
                if indexElem != len(matrixResult[indexList])-1 : charTemp += ";"
            charTemp += "\n"
            lignes.append(charTemp)
        sys = platform.system()
        if sys == "Linux" : path = f"{os.getcwd()}/CSVfiles/{table}.csv"
        elif sys == "Windows" : path = f"{os.getcwd()}\CSVfiles\{table}.csv"
        with open(path, "w", encoding="utf-8") as file :
            file.writelines(lignes)

def fillFromCSV() :
    """ Intègre des données à la base à partir de fichiers CSVs """
    tableList = ['artiste', 'album', 'titre', 'utilisateur', 'playlist', 'rassembler', 'posseder']
    for table in tableList :
        sys = platform.system()
        if sys == "Linux" : path = f"{os.getcwd()}/CSVfiles/{table}.csv"
        elif sys == "Windows" : path = f"{os.getcwd()}\CSVfiles\{table}.csv"
        with open(path, "r", newline='\n',encoding='utf-8') as csvFile :
            lignesEtud = csv.reader(csvFile,delimiter=';')
            for champs in lignesEtud :
                if table == "artiste" : executeRequest(insertArtiste(champs[1], champs[2]))
                elif table == "album" : executeRequest(insertAlbum(champs[1], champs[2], champs[3]))
                elif table == "titre" : executeRequest(insertTitre(champs[1], champs[2], int(champs[3]), int(champs[4])))
                elif table == "utilisateur" : executeRequest(insertUtilisateur(champs[1], champs[2]))
                elif table == "playlist" : executeRequest(insertPlaylist(champs[1], champs[2], champs[3], int(champs[4])))
                elif table == "rassembler" : executeRequest(insertRassembler(int(champs[1]), int(champs[2])))
                elif table == "posseder" : executeRequest(insertPosseder(int(champs[1]), int(champs[2])))
        attributsTab = getAttributesTab(table)
        printValuesInTable(attributsTab, table, True)
        print(f'Table "{table}" remplie')

if __name__ == '__main__':
    # Information du répertoure courant
    print(f"Répertoire courant : {os.listdir()}")
    # Connexion à la base, voire création si elle est inexistante
    initBase()
    listeChoix = [
        # Create 
        ("Insérer une valeur dans une table ", insertInTable),
        # Read
        ("Afficher les tables de la base ", printTables),
        ("Afficher les attributs d'une table ", printAttributesTable),
        # Read
        ("Mettre à jour une valeur dans une table ", updateInTable),
        # Delete
        ("Supprimer une valeur dans une table ", deleteInTable),
        ("Supprimer entièrement le contenu d'une table ", deleteAllTable),
        # CSV
        ("Enresgistrer les tables au format CSV", saveInCSV),
        ("Remplir les tables depuis les fichiers CSVs", fillFromCSV),
        # Debug
        ("Exécuter une requête SQL", executeSQL)
    ]
    fini = False
    while not(fini) :
        affMenuAction(listeChoix)
        try :
            choix = int(input("Quelle action choisissez-vous : "))
            if choix == len(listeChoix)+1 :
                fini = True
            elif choix >= 1 and choix <= len(listeChoix):
                labelInstruction, instruction = listeChoix[choix-1]
                instruction()
            else :
                print ("*** Choix non valide, recommencez!")
        except IndexError as e:
            print(e)
            print ('*** Choix non valide, recommencez!')
        except ValueError as e :
            print(e)
            print ('*** Entrez un entier SVP')
    print()
    print("Interface CLI fermé. Au revoir")