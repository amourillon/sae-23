-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 11, 2023 at 08:44 AM
-- Server version: 8.0.31
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `musique`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

DROP TABLE IF EXISTS `album`;
CREATE TABLE IF NOT EXISTS `album` (
  `numAlbum` int NOT NULL AUTO_INCREMENT,
  `nomAlbum` varchar(50) NOT NULL,
  `classication` enum('Single','EP','Album') NOT NULL,
  `dateSortie` date NOT NULL,
  PRIMARY KEY (`numAlbum`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`numAlbum`, `nomAlbum`, `classication`, `dateSortie`) VALUES
(1, 'CLOUDS', 'Album', '2021-02-18'),
(2, 'Origins', 'Album', '2019-01-18'),
(3, 'X Games', 'EP', '2018-10-19');

-- --------------------------------------------------------

--
-- Table structure for table `artiste`
--

DROP TABLE IF EXISTS `artiste`;
CREATE TABLE IF NOT EXISTS `artiste` (
  `numArtiste` int NOT NULL AUTO_INCREMENT,
  `nomArtiste` varchar(50) NOT NULL,
  `descripArt` varchar(100) NOT NULL,
  PRIMARY KEY (`numArtiste`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `artiste`
--

INSERT INTO `artiste` (`numArtiste`, `nomArtiste`, `descripArt`) VALUES
(1, 'NF', 'Rappeur américain'),
(2, 'Danger', 'DJ Français'),
(3, 'Au/Ra', 'Pop US');

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

DROP TABLE IF EXISTS `playlist`;
CREATE TABLE IF NOT EXISTS `playlist` (
  `numPlaylist` int NOT NULL AUTO_INCREMENT,
  `nomPlaylist` varchar(50) NOT NULL,
  `theme` varchar(100) NOT NULL,
  `dateCreation` date NOT NULL DEFAULT (curdate()),
  `numCreateur` int NOT NULL,
  PRIMARY KEY (`numPlaylist`),
  KEY `constituer` (`numCreateur`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`numPlaylist`, `nomPlaylist`, `theme`, `dateCreation`, `numCreateur`) VALUES
(1, 'US Rap', 'Rap US uniquement', '2023-05-11', 1),
(2, 'Indé', 'Musiques indépendantes sympa', '2023-05-11', 1),
(3, 'Gonna Hit The Bench', 'Boom Boom pour la muscu', '2023-05-11', 2);

-- --------------------------------------------------------

--
-- Table structure for table `posseder`
--

DROP TABLE IF EXISTS `posseder`;
CREATE TABLE IF NOT EXISTS `posseder` (
  `numInteraAlAr` int NOT NULL AUTO_INCREMENT,
  `numAlbum` int NOT NULL,
  `numArtiste` int NOT NULL,
  PRIMARY KEY (`numInteraAlAr`),
  KEY `possederSideAl` (`numAlbum`),
  KEY `possederSideAr` (`numArtiste`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `posseder`
--

INSERT INTO `posseder` (`numInteraAlAr`, `numAlbum`, `numArtiste`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rassembler`
--

DROP TABLE IF EXISTS `rassembler`;
CREATE TABLE IF NOT EXISTS `rassembler` (
  `numInteraTiPl` int NOT NULL AUTO_INCREMENT,
  `numTitre` int NOT NULL,
  `numPlaylist` int NOT NULL,
  PRIMARY KEY (`numInteraTiPl`),
  KEY `rassemblerSideT` (`numTitre`),
  KEY `rassemblerSideP` (`numPlaylist`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `rassembler`
--

INSERT INTO `rassembler` (`numInteraTiPl`, `numTitre`, `numPlaylist`) VALUES
(1, 1, 3),
(2, 2, 3),
(3, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `titre`
--

DROP TABLE IF EXISTS `titre`;
CREATE TABLE IF NOT EXISTS `titre` (
  `numTitre` int NOT NULL AUTO_INCREMENT,
  `nomTitre` varchar(50) NOT NULL,
  `duree` time NOT NULL,
  `numAlbum` int NOT NULL,
  `numArtiste` int NOT NULL,
  PRIMARY KEY (`numTitre`),
  KEY `grouper` (`numAlbum`),
  KEY `composer` (`numArtiste`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `titre`
--

INSERT INTO `titre` (`numTitre`, `nomTitre`, `duree`, `numAlbum`, `numArtiste`) VALUES
(1, 'CLOUDS', '00:04:03', 1, 1),
(2, '22:39', '00:04:12', 2, 2),
(3, 'Panic Room', '00:03:58', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `numUtil` int NOT NULL AUTO_INCREMENT,
  `pseudonyme` varchar(50) NOT NULL,
  `descripUtil` varchar(100) NOT NULL,
  PRIMARY KEY (`numUtil`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`numUtil`, `pseudonyme`, `descripUtil`) VALUES
(1, 'PulseStar5k', 'THE Admin, aime beaucoup de st'),
(2, 'Triston', 'C est Tristan, il aime le thon'),
(3, 'Il_Duccini', 'TheBestProfDInfo');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `playlist`
--
ALTER TABLE `playlist`
  ADD CONSTRAINT `constituer` FOREIGN KEY (`numCreateur`) REFERENCES `utilisateur` (`numUtil`) ON DELETE CASCADE;

--
-- Constraints for table `posseder`
--
ALTER TABLE `posseder`
  ADD CONSTRAINT `possederSideAl` FOREIGN KEY (`numAlbum`) REFERENCES `album` (`numAlbum`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `possederSideAr` FOREIGN KEY (`numArtiste`) REFERENCES `artiste` (`numArtiste`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rassembler`
--
ALTER TABLE `rassembler`
  ADD CONSTRAINT `rassemblerSideP` FOREIGN KEY (`numPlaylist`) REFERENCES `playlist` (`numPlaylist`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rassemblerSideT` FOREIGN KEY (`numTitre`) REFERENCES `titre` (`numTitre`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `titre`
--
ALTER TABLE `titre`
  ADD CONSTRAINT `composer` FOREIGN KEY (`numArtiste`) REFERENCES `artiste` (`numArtiste`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `grouper` FOREIGN KEY (`numAlbum`) REFERENCES `album` (`numAlbum`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
