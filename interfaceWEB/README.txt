Ce fichier rassemble quelques informations utiles à l'usage des fichiers ci-joints

Bibliothèques Python utilisées :
    - pymysql
    - os
    - platform
    - csv
    ==> Normalement c'est bibliothèque sont déjà installé avec Python

Contenu de fichier de configuration :
    - Le fichier de configuration contient les informations nécessaires à la connexion au serveur.
    Veuillez bien les remplir avec vos paramètres propres
    - Sur une configuration Windows 10 utilisant WampServer64, les paramètres connseillés sont :
        type : MySQL
        nombase : musique
        host : localhost
        user : root
        pass : 
        port : 3306
    - Sur Linux des ordinateurs de l'IUT, les paramètres connseillés sont :
        type : MySQL
        nombase : musique
        host : localhost
        user : admin
        pass : admin
        port : 3306

Procédure d'utilisation : 
    - Lancez votre service de base de donnée (Wamp, mysql, etc ...), puis le fichier "mainWeb.py" (python mainWeb.py OU python3 mainWeb.py)
    - Si la base n'a jamais été créer sur votre machine, répondez "Y" pour créer la base. Il est aussi conseillé d'intégrer les données minimales, comme proposé
    - Rendez-vous ensuite sur votre navigateur pour utiliser l'interface web (adresse et port en fonction du ficher "config.txt")

! ! !   Lisez les conseils d'utilisation de la page d'accueil avant l'utilisation de l'interface   ! ! !

Informations complémentaires :
    - La base de données a subi quelques remaniement sur sa structure, notament dans la liaision des tables artiste et album, ainsi que la création de la table sondage et sa liaison au reste de la base
    - La base de donnée est générée "vide" (à l'exception de quelques données) ==> appuyer sur "Importer" pour intégrer quelques données
    - Attention au bouton "Exporter" ==> celui écrase les anciennes données des fichiers CSV. Assurez vous du contenu de la base de donnée avant de l'utiliser