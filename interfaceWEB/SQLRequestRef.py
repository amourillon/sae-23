# Axel MOURILLON - SAE 2.3 

_requetes = {
    # --- Create : requêtes de création de la base et des tables, et insertions de valeurs de bases
    # Nettoyage et usage de la base de données
    "drop" : """DROP DATABASE IF EXISTS musique ;""",
    "createBase" : """CREATE DATABASE IF NOT EXISTS musique DEFAULT CHARACTER SET utf8 ;""",
    "use" : """USE musique""",
    # Création des tables
    "createTableAlbum" : """CREATE TABLE album ( \
        numAlbum INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomAlbum VARCHAR(75) NOT NULL,\
        classification ENUM('Album', 'EP', 'Single') NOT NULL,\
        dateSortie DATE NOT NULL, \
	    numArtiste INTEGER DEFAULT 1) ENGINE=InnoDB ;""",
    "createTableArtiste" :"""CREATE TABLE artiste ( \
        numArtiste INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomArtiste VARCHAR(75) NOT NULL,\
        descripArt VARCHAR(250) NOT NULL) ENGINE=InnoDB ;""", 
    "createTableTitre" :"""CREATE TABLE titre ( \
        numTitre INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomTitre VARCHAR(75) NOT NULL,\
        duree TIME NOT NULL, \
        numAlbum INTEGER DEFAULT 1, \
        numArtiste INTEGER DEFAULT 1) ENGINE=InnoDB ;""",
    "createTablePlaylist" :"""CREATE TABLE playlist ( \
        numPlaylist INTEGER PRIMARY KEY AUTO_INCREMENT,\
        nomPlaylist VARCHAR(75) NOT NULL,\
        theme VARCHAR(250) NOT NULL,\
        dateCreation DATE NOT NULL DEFAULT (CURRENT_DATE), \
        numCreateur INTEGER NOT NULL DEFAULT 1) ENGINE=InnoDB ;""",
    "createTableUtilisateur" :"""CREATE TABLE utilisateur (\
        numUtil INTEGER PRIMARY KEY AUTO_INCREMENT,\
        pseudonyme VARCHAR(75) NOT NULL,\
        descripUtil VARCHAR(250) NOT NULL) ENGINE=InnoDB ;""",
    "createTableRassembler" :"""CREATE TABLE rassembler ( \
        numInteraTiPl INTEGER UNIQUE AUTO_INCREMENT, \
        numTitre INTEGER NOT NULL,\
        numPlaylist INTEGER NOT NULL, \
        PRIMARY KEY (numTitre, numPlaylist) ) ENGINE=InnoDB ;""",
    "createTableSondage" :"""CREATE TABLE sondage (\
        numSondage INTEGER UNIQUE AUTO_INCREMENT, \
        numPlaylist_1 INTEGER,\
        nbVotePl_1 INTEGER DEFAULT 0,\
        numPlaylist_2 INTEGER,\
        nbVotePl_2 INTEGER DEFAULT 0,\
        numPlaylist_3 INTEGER,\
        nbVotePl_3 INTEGER DEFAULT 0, \
        PRIMARY KEY (numPlaylist_1, numPlaylist_2, numPlaylist_3) ) ENGINE=InnoDB ;""",
    # Création des relations
    "createRelationGrouper" : """ALTER TABLE titre ADD CONSTRAINT grouper \
        FOREIGN KEY (numAlbum) \
        REFERENCES album(numAlbum) ON DELETE CASCADE ON UPDATE CASCADE ;""",
    "createRelationComposer" : """ALTER TABLE titre ADD CONSTRAINT composer \
        FOREIGN KEY (numArtiste) \
        REFERENCES artiste(numArtiste) ON DELETE CASCADE ON UPDATE CASCADE ;""",
    "createRelationPosseder" : """ALTER TABLE album ADD CONSTRAINT posseder \
        FOREIGN KEY (numArtiste) \
        REFERENCES artiste(numArtiste) ON DELETE CASCADE ON UPDATE CASCADE ;""",
    "createRelationRassemblerSideT" : """ALTER TABLE rassembler ADD CONSTRAINT rassemblerSideT \
        FOREIGN KEY (numTitre) \
        REFERENCES titre(numTitre) ON DELETE CASCADE ON UPDATE CASCADE ;""",
    "createRelationRassemblerSideP" : """ALTER TABLE rassembler ADD CONSTRAINT rassemblerSideP \
        FOREIGN KEY (numPlaylist) \
        REFERENCES playlist(numPlaylist) ON DELETE CASCADE ON UPDATE CASCADE ;""",
    "createRelationConstituer" : """ALTER TABLE playlist ADD CONSTRAINT constituer \
        FOREIGN KEY (numCreateur) \
        REFERENCES utilisateur(numUtil) ON DELETE CASCADE ON UPDATE CASCADE ;""",
    "createRelationSondagePl_1" : """ALTER TABLE sondage ADD  CONSTRAINT sondagePl_1 \
        FOREIGN KEY (numPlaylist_1)  \
        REFERENCES playlist(numPlaylist) ON DELETE CASCADE ON UPDATE CASCADE;""",
    "createRelationSondagePl_2" : """ALTER TABLE sondage ADD  CONSTRAINT sondagePl_2 \
        FOREIGN KEY (numPlaylist_2)  \
        REFERENCES playlist(numPlaylist) ON DELETE CASCADE ON UPDATE CASCADE;""",
    "createRelationSondagePl_3" : """ALTER TABLE sondage ADD  CONSTRAINT sondagePl_3 \
        FOREIGN KEY (numPlaylist_3)  \
        REFERENCES playlist(numPlaylist) ON DELETE CASCADE ON UPDATE CASCADE;""",
        
    # Insertion de valeurs par défauts - INUTILE pour le moment
    "defaultInsertionsArtiste" : """INSERT INTO artiste (nomArtiste,descripArt) VALUES \
        ('NF','Rappeur américain'),\
        ('Danger','DJ Français'),\
        ('Au/Ra','Pop US');""",
    "defaultInsertionsAlbum" : """INSERT INTO album (nomAlbum,classification,dateSortie) VALUES \
        ('CLOUDS','Album','2021-02-18'),\
        ('Origins','Album','2019-01-18'),\
        ('X Games','EP','2018-10-19');""",
    "defaultInsertionsTitre" : """INSERT INTO titre (nomTitre,duree,numAlbum,numArtiste) VALUES \
        ('CLOUDS','00:04:03',1,1),\
        ('22:39','00:04:12',2,2),\
        ('Panic Room','00:03:58',3,3);""",
    "defaultInsertionsUtilisateur" : """INSERT INTO utilisateur (pseudonyme,descripUtil) VALUES \
        ('PulseStar5k','THE Admin, aime beaucoup de styles différents (mais surtout l indé)'),\
        ('Triston','C est Tristan, il aime le thon et la phonk'),\
        ('Il_Duccini','TheBestProfDInfo');""",
    "defaultInsertionsPlaylist" : """INSERT INTO playlist (nomPlaylist,theme,numCreateur) VALUES \
        ('US Rap','Rap US uniquement',1),\
        ('Indé','Musiques indépendantes sympa',1),\
        ('Gonna Hit The Bench','Boom Boom pour la muscu',2);""",
    "defaultInsertionsRassembler" : """INSERT INTO rassembler (numTitre,numPlaylist) VALUES \
        (1,3),\
        (2,3),\
        (3,2);""",
        
    # Insertion par tables
    "insert" : """INSERT INTO {} ({}) VALUES ({});""",
    "insertArtisteWEB" : """INSERT INTO artiste ({}) VALUES ({});""",
    "insertAlbumWEB" : """INSERT INTO album ({}) VALUES ({});""",
    "insertTitreWEB" : """INSERT INTO titre ({}) VALUES ({});""",
    "insertPlaylistWEB" : """INSERT INTO playlist ({}) VALUES ({});""",
    "insertUtilisateurWEB" : """INSERT INTO utilisateur ({}) VALUES ({});""",
    "insertRassemblerWEB" : """INSERT INTO rassembler ({}) VALUES ({});""",
    "insertSondage" : """INSERT INTO sondage (numPlaylist_1, numPlaylist_2, numPlaylist_3) VALUES ({}, {}, {});""",
    
    # --- Read : requêtes de lecture de la base
    "showTables" : "SHOW TABLES ;",
    "get" : """SELECT {} FROM {} ;""",
    "getAll" : """SELECT * FROM {} ;""",
    "getLike" : """SELECT * FROM {} WHERE {} LIKE "%{}%";""",
    "getSorted" : """SELECT * FROM {} ORDER BY {} ;""",
    # Variantes dû aux jointures :
    "getAll_Titre" : """SELECT numTitre, nomTitre, duree, nomAlbum, nomArtiste FROM titre JOIN album USING(numAlbum) JOIN artiste ON artiste.numArtiste = titre.numArtiste {} ; """,
    "getAll_Album" : """SELECT numAlbum, nomAlbum, nomArtiste, classification, dateSortie FROM album JOIN artiste USING(numArtiste) {} ; """,
    "getAll_Playlist" : """SELECT numPlaylist, nomPlaylist, pseudonyme, theme, dateCreation FROM playlist JOIN utilisateur ON playlist.numCreateur = utilisateur.numUtil {} ; """,
    "getAllTitreInPlaylist" : """SELECT numTitre, nomTitre, duree, nomArtiste, nomAlbum FROM titre \
        JOIN rassembler USING(numTitre) JOIN playlist USING(numPlaylist) JOIN album USING(numAlbum) JOIN artiste ON artiste.numArtiste = titre.numArtiste\
        WHERE numPlaylist = "{}";""",
    "getDropMenuUser" : """SELECT numUtil, pseudonyme FROM utilisateur ORDER BY pseudonyme;""",
    "getDropMenuArtiste" : """SELECT numArtiste, nomArtiste FROM artiste ORDER BY nomArtiste;""",
    "getDropMenuAlbum" : """SELECT numAlbum, nomAlbum FROM album ORDER BY nomAlbum;""",
    "getDropMenuPlaylist" : """SELECT numPlaylist, nomPlaylist FROM playlist ORDER BY nomPlaylist;""",
    "getDropMenuTitre" : """SELECT numTitre, nomTitre FROM titre ORDER BY nomTitre;""",
    "getNamePlaylistById" : """SELECT nomPlaylist FROM playlist WHERE numPlaylist = "{}" ORDER BY nomPlaylist;""",
    # Sélection par ID :
    "getByID" : """SELECT * FROM {} WHERE {};""",
    # Sélections pour le sondage :
    "getPlaylistNamePlById" : """SELECT nomPlaylist FROM playlist WHERE numPlaylist = "{}"; """,
    "getPlaylistNameCreaById" : """SELECT pseudonyme FROM utilisateur JOIN playlist ON playlist.numCreateur = utilisateur.numUtil WHERE playlist.numPlaylist = '{}'; """,
    "getPlaylistDateCreateById" : """SELECT dateCreation FROM playlist WHERE numPlaylist = "{}"; """,
    "getPlaylistThemeById" : """SELECT theme FROM playlist WHERE numPlaylist = "{}"; """,
    "getActualSondage" : """SELECT numSondage, numPlaylist_1, nbVotePl_1, numPlaylist_2, nbVotePl_2, numPlaylist_3, nbVotePl_3 FROM sondage WHERE numSondage IN (SELECT MAX(numSondage) FROM sondage);""",
    "getPastSondage" : """SELECT * FROM sondage WHERE numSondage NOT IN (SELECT MAX(numSondage) FROM sondage) ORDER BY numSondage;""",
    # Récupération d'IDs max
    "getMaxSondageId" : """SELECT MAX(numSondage) FROM sondage;""",
    "getMaxIDTitre" : """ SELECT MAX(numTitre) FROM titre; """,
    "getMaxIDArtiste" : """ SELECT MAX(numArtiste) FROM artiste; """,
    "getMaxIDAlbum" : """ SELECT MAX(numAlbum) FROM album; """,
    "getMaxIDPlaylist" : """ SELECT MAX(numPlaylist) FROM playlist; """,
    "getMaxIDUser" : """ SELECT MAX(numUtil) FROM utilisateur; """,
    
    # --- Update : requête de mise à jour de la base
    "update" : """UPDATE {} SET {} = "{}" WHERE {} = "{}";""",
    "updateTitreByID" : """UPDATE titre SET nomTitre = "{}", duree = "{}", numAlbum = "{}", numArtiste = "{}" WHERE numTitre = "{}";""",
    "updateArtisteByID" : """UPDATE artiste SET nomArtiste = "{}", descripArt = "{}" WHERE numArtiste = "{}";""",
    "updateAlbumByID" : """UPDATE album SET nomAlbum = "{}", classification = "{}", dateSortie = "{}", numArtiste = "{}" WHERE numAlbum = "{}";""",
    "updatePlaylistByID" : """UPDATE playlist SET nomPlaylist = "{}", theme = "{}", numCreateur = "{}" WHERE numPlaylist = "{}"; """,
    "updateUserByID" : """UPDATE utilisateur SET pseudonyme = "{}", descripUtil = "{}" WHERE numUtil = "{}";""",
    # UPDATES POUR LE SONDAGE :
    "updateSondage" : """UPDATE sondage SET nbVotePl_{} = nbVotePl_{}+1 WHERE numSondage = "{}"; """,
    
    # --- Delete : requêtes de suppression dans la base
    "delete" : """DELETE FROM {} where {} = "{}";""" ,
    "resetTable" : """DELETE FROM {};""",
    "deleteInPlaylist" : """ DELETE rassembler FROM rassembler
        JOIN titre ON titre.numTitre = rassembler.numTitre
        JOIN playlist ON playlist.numPlaylist = rassembler.numPlaylist
        WHERE rassembler.numPlaylist = {} AND {} = "{}" ;""",
    "dropTable" : """DROP TABLE {};""",

    # --- CSV : requêtes nécessaire à l'extraction des données depuis un fichier CSV
    "insertArtiste_CSV" : """INSERT INTO artiste (nomArtiste,descripArt) VALUES ("{}", "{}");""",
    "insertAlbum_CSV" : """INSERT INTO album (nomAlbum,classification,dateSortie,numArtiste) VALUES ("{}", "{}", "{}", "{}");""",
    "insertTitre_CSV" : """INSERT INTO titre (nomTitre,duree,numAlbum,numArtiste) VALUES ("{}", "{}", {}, {});""",
    "insertUtilisateurCLI_CSV" : """INSERT INTO utilisateur (pseudonyme,descripUtil) VALUES ("{}", "{}");""",
    "insertPlaylist_CSV" : """INSERT INTO playlist (nomPlaylist,theme,dateCreation,numCreateur) VALUES ("{}", "{}", "{}", {});""",
    "insertRassembler_CSV" : """INSERT INTO rassembler (numTitre,numPlaylist) VALUES ({}, {});""",
    "insertSondage_CSV" : """INSERT INTO sondage (numPlaylist_1, nbVotePl_1, numPlaylist_2, nbVotePl_2, numPlaylist_3, nbVotePl_3) VALUES ({}, {}, {}, {}, {}, {});""",
}

# Extraction de données à partir d'un fichier CSV
#   Routines renvoyant la requête d'insertion de données en fonction des attributs de leurs tables respectives
def insertArtiste_CSV(nomArtiste : str, descripArt : str) :
    return _requetes["insertArtiste_CSV"].format(nomArtiste, descripArt)
def insertAlbum_CSV(nomAlbum : str , classification : str, dateSortie : str, numArtiste : str ) :
    return _requetes["insertAlbum_CSV"].format(nomAlbum, classification, dateSortie, numArtiste)
def insertTitre_CSV(nomTitre : str, duree : str, numAlbum : int, numArtiste : int) :
    return _requetes["insertTitre_CSV"].format(nomTitre, duree, numAlbum, numArtiste)
def insertUtilisateurCLI_CSV(pseudonyme : str, descripUtil : str) :
    return _requetes["insertUtilisateurCLI_CSV"].format(pseudonyme, descripUtil)
def insertPlaylist_CSV(nomPlaylist : str, theme : str, dateCreation : str, numCreateur : int) :
    return _requetes["insertPlaylist_CSV"].format(nomPlaylist, theme, dateCreation, numCreateur)
def insertRassembler_CSV(numTitre : int, numPlaylist : int) :
    return _requetes["insertRassembler_CSV"].format(numTitre, numPlaylist)
def insertSondage_CSV(numPlaylist_1 : int, nbVotePl_1 : int, numPlaylist_2 : int, nbVotePl_2 : int, numPlaylist_3 : int, nbVotePl_3 : int) :
    return _requetes["insertSondage_CSV"].format(numPlaylist_1, nbVotePl_1, numPlaylist_2, nbVotePl_2, numPlaylist_3, nbVotePl_3)