# Axel MOURILLON - SAE 2.3 
# Mémo => se connecter à la base dans le terminal : mysql -h localhost -u root –p

# Importation
import pymysql

def getConnectionParam(pathConfigFile = "config.txt") : 
    """ Récupère les paramètres de connexion au serveur et à la base depuis le fichier de configuration. Renvoie
    ces paramètres sous une forme exploitable

    Args:
        pathConfigFile (str, optional): chemin vers le fichier de configuration. Defaults to "config.txt".

    Returns:
        tuple: tuple rassemblant les différents paramètres de connexion
    """
    # Paramètres de connexion par défaut :
    typeBase = "MySQL"
    base = "musique"
    server = "localhost"
    user = "root"
    mdp = ""
    numPort = 3306 # Port MySQL standard 
    try :
        with open(pathConfigFile) as file :
            for ligne in file :
                inLine = ligne.split(" : ")
                if inLine[0] == "type" : typeBase = inLine[1][:-1]
                elif inLine[0] == "nombase" : base = inLine[1][:-1]
                elif inLine[0] == "host" :  server = inLine[1][:-1]
                elif inLine[0] == "user" :  user = inLine[1][:-1]
                elif inLine[0] == "pass" : mdp = inLine[1][:-1]
                elif inLine[0] == "port" : 
                    if len(inLine[1][:-1]) >= 4 : numPort = inLine[1][:-1] # Empêche les erreurs s'il n'y pas de lignes après
                    else : numPort = inLine[1]
                    numPort = int(numPort)
    except FileNotFoundError as e :
        print("Le fichier 'config.txt' est introuvable : utilisation des valeurs par défaut ... ")
    return typeBase, base, server, user, mdp, numPort

def dbConnect(databaseExists : bool) : # Ne gère que MySQL
    """ Connexion au serveur. Se connecte particulièrement à la base de données mentionnée en fonction du booléen
    en paramètre. Renvoie l'objet de connexion à la base et son curseur associé

    Args:
        databaseExists (bool): Si True, tentative de connexion à la base de données ; si False, se connecte unique au serveur hôte

    Returns:
        tuple: tuple rassemblant l'objet de connexion à la base et son curseur associé
    """
    # Récupération des paramètres de connexion
    typeBase, base, hostName, userNum, mdp, portNum = getConnectionParam()
    # Teste du bool en paramètre
    if databaseExists : database = pymysql.connect(host=hostName, charset="utf8", user=userNum, passwd=mdp, db=base, port=portNum)
    else : database = pymysql.connect(host=hostName, charset="utf8", user=userNum, passwd=mdp, port=portNum)
    return (database, database.cursor())

