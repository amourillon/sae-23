# Axel MOURILLON - SAE 2.3 

# Importations
import pymysql, os, platform, csv
from configDatabase import *
from SQLRequestRef import _requetes, \
    insertUtilisateurCLI_CSV, insertArtiste_CSV, insertAlbum_CSV, insertTitre_CSV, \
    insertPlaylist_CSV, insertRassembler_CSV, insertSondage_CSV

def creationTables(cursor) :
    """ Rassemble les exécutions de requêtes de création de table

    Args:
        cursor : curseur (ou query)
    """
    cursor.execute(_requetes["createTableArtiste"])
    cursor.execute(_requetes["createTableAlbum"])
    cursor.execute(_requetes["createTableTitre"])
    cursor.execute(_requetes["createTablePlaylist"])
    cursor.execute(_requetes["createTableUtilisateur"])
    cursor.execute(_requetes["createTableRassembler"])
    cursor.execute(_requetes["createTableSondage"])

def creationRelations(cursor) :
    """ Rassemble les exécutions de requêtes de création de relation

    Args:
        cursor : curseur (ou query)
    """
    cursor.execute(_requetes["createRelationGrouper"])
    cursor.execute(_requetes["createRelationComposer"])
    cursor.execute(_requetes["createRelationPosseder"])
    cursor.execute(_requetes["createRelationRassemblerSideT"])
    cursor.execute(_requetes["createRelationRassemblerSideP"])
    cursor.execute(_requetes["createRelationConstituer"])
    cursor.execute(_requetes["createRelationSondagePl_1"])
    cursor.execute(_requetes["createRelationSondagePl_2"])
    cursor.execute(_requetes["createRelationSondagePl_3"])

def defautInsert(cursor) :
    """ Rassemble les exécutions de requêtes d'insertion de données prédéfinies dans la base

    Args:
        cursor : curseur (ou query)
    """
    cursor.execute(_requetes["insertArtisteWEB"].format("numArtiste,nomArtiste,descripArt", """ 1,"-inconnu-","-À utiliser quand vous ne connaissez pas l'artiste-" """))
    cursor.execute(_requetes["insertAlbumWEB"].format("numAlbum,nomAlbum,classification,dateSortie,numArtiste", """ 1,"-inconnu-","Single","0000-00-00","1" """))
    cursor.execute(_requetes["insertUtilisateurWEB"].format("numUtil,pseudonyme,descripUtil", """ 1, "Invité", "-utilisateur anonyme-" """))

def createBaseMySQL() :
    """ Créer la base lorsque que celle-ci n'existe pas """
    # Connexion au serveur, sans chercher à se connecter à la base
    _db, _cursor = dbConnect(False)
    # Commande préalable, pour une création de table propre
    _cursor.execute(_requetes["drop"])
    _cursor.execute(_requetes["createBase"])
    _cursor.execute(_requetes["use"])
    # Créations des tables :
    creationTables(_cursor)
    # Créations des relations :
    creationRelations(_cursor)
    # Insertion de valeurs par défaut : présente pour tester la base, ou la créée avec des valeurs depuis python
    # defautInsert(_cursor)
    # Sauvegarde des commandes exécutées précédemment
    _db.commit()

def executeRequest(req : str) :
    """ Exécute une requête SQL, la sauvegarde, et renvoie son compte-rendu d'exécution et son résultat

    Args:
        req (str): requête à exécuter

    Returns:
        tuple: tuple rassemblant le compte-rendu d'exécution et le résultat de la requête
    """
    _db, _cursor = dbConnect(True)
    cr = _cursor.execute(req)
    res = _cursor.fetchall()
    _db.commit()
    return cr,res

def executeSQL() :
    """ Interface de test et de debug, pour l'exécution d'une requête SQL directement depuis le CLI sur la base """
    req = input("Entrez la requête à exécuter : ")
    try :
        cr, result = executeRequest(req)
        print("Compte-rendu d'exécution : ", cr)
        print("Résultat : ", result)
    except Exception as e :
        print("Erreur de la requête : ", e)

def tryConnect() : 
    """ Teste la connexion au serveur, et renvoie le résultat

    Returns:
        Le résultat est une erreur en cas d'échec de connexion, et la liste des tables tout s'est bien passé
    """
    # Tentative de connexion à la base de données
    _db, _cursor = dbConnect(True)
    # Tentative d'exécution d'une requête simple
    result = _cursor.execute(_requetes["showTables"])
    return result

def getTables() :
    """ Interroge la base de données et renvoie la liste des tables

    Returns:
        list: liste de chaîne de caractères, ressemblant les tables de la base
    """
    cr, result = executeRequest(_requetes["showTables"])
    listeTables = []
    for element in result :
        listeTables.append(element[0])
    return listeTables

def getAttributesTab(table) :
    """ Renvoie la liste des attributs de la table en paramètre

    Args:
        table (str): table dont on veut les attributs

    Returns:
        list: liste de chaîne de caractères
    """
    if table != "0" :
        if table.lower() == "artiste" : listeAttr = ["numArtiste", "nomArtiste", "descripArt"]
        elif table.lower() == "album" : listeAttr = ["numAlbum", "nomAlbum", "classification", "dateSortie", "numArtiste"]
        elif table.lower() == "titre" : listeAttr = ["numTitre", "nomTitre", "duree", "numAlbum", "numArtiste"]
        elif table.lower() == "utilisateur" : listeAttr = ["numUtil", "pseudonyme", "descripUtil"]
        elif table.lower() == "playlist" : listeAttr = ["numPlaylist", "nomPlaylist", "theme", "dateCreation", "numCreateur"]
        elif table.lower() == "rassembler" : listeAttr = ["numInteraTiPl", "numTitre", "numPlaylist"]
        return listeAttr

def resultToMatrix(result) :
    """ Transforme un tuple de tuple en liste de liste

    Args:
        result (tuple): tuple de tuple rassemblant les résultats de la requête

    Returns:
        list: liste de listes, ces-dernières contiennent les résultats
    """
    matrixResult = []
    for element in result :
        tempList = []
        for inElement in element :
            tempList.append(inElement)
        matrixResult.append(tempList)
    return matrixResult

def fillFromCSV() :
    """ Intègre des données à la base à partir de fichiers CSVs """
    tableList = ['artiste', 'album', 'titre', 'utilisateur', 'playlist', 'rassembler', 'sondage']
    for table in tableList :
        sys = platform.system()
        if sys == "Linux" : path = f"{os.getcwd()}/CSVfiles/{table}.csv"
        elif sys == "Windows" : path = f"{os.getcwd()}\CSVfiles\{table}.csv"
        with open(path, "r", newline='\n',encoding='utf-8') as csvFile :
            lignesEtud = csv.reader(csvFile,delimiter=';')
            for champs in lignesEtud :
                if table == "artiste" and champs[1] != "-inconnu-" : executeRequest(insertArtiste_CSV(champs[1], champs[2]))
                elif table == "album" and champs[1] != "-inconnu-" : executeRequest(insertAlbum_CSV(champs[1], champs[2], champs[3], champs[4]))
                elif table == "titre" : executeRequest(insertTitre_CSV(champs[1], champs[2], int(champs[3]), int(champs[4])))
                elif table == "utilisateur" and champs[1] != "Invité" : executeRequest(insertUtilisateurCLI_CSV(champs[1], champs[2]))
                elif table == "playlist" : executeRequest(insertPlaylist_CSV(champs[1], champs[2], champs[3], int(champs[4])))
                elif table == "rassembler" : executeRequest(insertRassembler_CSV(int(champs[1]), int(champs[2])))
                elif table == "sondage" : executeRequest(insertSondage_CSV(champs[1], champs[2], champs[3], champs[4], champs[5], champs[6]))
        # print(f'Table "{table}" remplie')

def saveInCSV():
    """ Extrait les données de toutes les tables dans des fichiers CSVs """
    for table in getTables() :
        cr,result = executeRequest(_requetes["getAll"].format(table))
        matrixResult = resultToMatrix(result)
        lignes = []
        for indexList in range(len(matrixResult)) :
            charTemp = ""
            for indexElem in range(len(matrixResult[indexList])) :
                charTemp += str(matrixResult[indexList][indexElem])
                if indexElem != len(matrixResult[indexList])-1 : charTemp += ";"
            charTemp += "\n"
            lignes.append(charTemp)
        sys = platform.system()
        if sys == "Linux" : path = f"{os.getcwd()}/CSVfiles/{table}.csv"
        elif sys == "Windows" : path = f"{os.getcwd()}\CSVfiles\{table}.csv"
        with open(path, "w", encoding="utf-8") as file :
            file.writelines(lignes)

def attrToName_Titre(attributs : list) :
    for index in range(len(attributs)) :
        if attributs[index] == "numTitre" : attributs[index] = "ID"
        elif attributs[index] == "nomTitre" : attributs[index] = "Titre"
        elif attributs[index] == "duree" : attributs[index] = "Durée"
        elif attributs[index] == "nomAlbum" : attributs[index] = "Album"
        elif attributs[index] == "nomArtiste" : attributs[index] = "Artiste ou groupe"
def attrToName_Artiste(attributs : list) :
    for index in range(len(attributs)) :
        if attributs[index] == "numArtiste" : attributs[index] = "ID"
        elif attributs[index] == "nomArtiste" : attributs[index] = "Artiste ou groupe"
        elif attributs[index] == "descripArt" : attributs[index] = "Description"
def attrToName_Album(attributs : list) :
    for index in range(len(attributs)) :
        if attributs[index] == "numAlbum" : attributs[index] = "ID"
        elif attributs[index] == "nomAlbum" : attributs[index] = "Album"
        elif attributs[index] == "nomArtiste" : attributs[index] = "Artiste ou groupe"
        elif attributs[index] == "classification" : attributs[index] = "Classification"
        elif attributs[index] == "dateSortie" : attributs[index] = "Date de sortie"
def attrToName_Playlist(attributs : list) :
    for index in range(len(attributs)) :
        if attributs[index] == "numPlaylist" : attributs[index] = "ID"
        elif attributs[index] == "nomPlaylist" : attributs[index] = "Playlist"
        elif attributs[index] == "theme" : attributs[index] = "Thème(s)"
        elif attributs[index] == "dateCreation" : attributs[index] = "Date de création"
        elif attributs[index] == "pseudonyme" : attributs[index] = "Créateur"
def attrToName_User(attributs : list) :
    for index in range(len(attributs)) :
        if attributs[index] == "numUtil" : attributs[index] = "ID"
        elif attributs[index] == "pseudonyme" : attributs[index] = "Utilisateur"
        elif attributs[index] == "descripUtil" : attributs[index] = "Description"
def attrToName(attributs : list, table : str) :
    if table == "titre" : attrToName_Titre(attributs)
    elif table == "artiste" : attrToName_Artiste(attributs)
    elif table == "album" : attrToName_Album(attributs)
    elif table == "playlist" : attrToName_Playlist(attributs)
    elif table == "utilisateur" : attrToName_User(attributs)
### ---------------------------------------------------------------------------------------------
### --- Requêtes d'affichage de l'entiereté des tables et des pages --- ### 
def reqAllInTable(table : str) :
    cr, res = executeRequest(_requetes["getAll"].format(table))
    attr = getAttributesTab(table)
    attrToName(attr, table)
    return res, attr
def reqAllInTable_Titre() :
    cr, res = executeRequest(_requetes["getAll_Titre"].format(""))
    attr = ["numTitre", "nomTitre", "duree", "nomAlbum", "nomArtiste"]
    attrToName(attr, "titre")
    return res, attr
def reqAllInTable_Album() :
    cr, res = executeRequest(_requetes["getAll_Album"].format(""))
    attr = ["numAlbum", "nomAlbum", "nomArtiste", "classification", "dateSortie"]
    attrToName(attr, "album")
    return res, attr
def reqAllInTable_Playlist() :
    cr, res = executeRequest(_requetes["getAll_Playlist"].format(""))
    attr = ["numPlaylist", "nomPlaylist", "pseudonyme", "theme", "dateCreation"]
    attrToName(attr, "playlist")
    return res, attr
def reqAllTitreInPlaylist(numPlaylist : str) :
    cr, res = executeRequest(_requetes["getAllTitreInPlaylist"].format(numPlaylist))
    attr = ["numTitre", "nomTitre", "duree", "nomArtiste", "nomAlbum"]
    attrToName(attr, "titre")
    return res, attr
def reqNamePlaylist(numPlaylist : str) :
    cr, res = executeRequest(_requetes["getNamePlaylistById"].format(numPlaylist))
    return res[0][0]
### ---------------------------------------------------------------------------------------------
### --- Requêtes de recherches par nom en fonction des tables et des pages --- ### 
def reqSearchByNames(table : str, dictSearch : dict) :
    # Définition de "colName" et de "search"
    if table == "artiste" : 
        colName = "nomArtiste"
        search = dictSearch["searchArtiste"]
    elif table == "playlist" : 
        colName = "nomPlaylist"
        search = dictSearch["searchPlaylist"]
    elif table == "utilisateur" : 
        colName = "pseudonyme"
        search = dictSearch["searchUser"]
    # Requête
    cr, res = executeRequest(_requetes["getLike"].format(table, colName, search))
    attr = getAttributesTab(table)
    attrToName(attr, table)
    return res, attr
def reqSearchByNames_Titre(dictSearch : dict) :
    charToFormat = f"""WHERE nomTitre LIKE '%{dictSearch["searchTitre"]}%'"""
    cr, res = executeRequest(_requetes["getAll_Titre"].format(charToFormat))
    attr = ["numTitre", "nomTitre", "duree", "nomAlbum", "nomArtiste"]
    attrToName(attr, "titre")
    return res, attr
def reqSearchByNames_Album(dictSearch : dict) :
    charToFormat = f"""WHERE nomAlbum LIKE '%{dictSearch["searchAlbum"]}%'"""
    cr, res = executeRequest(_requetes["getAll_Album"].format(charToFormat))
    attr = ["numAlbum", "nomAlbum", "nomArtiste", "classification", "dateSortie"]
    attrToName(attr, "album")
    return res, attr
def reqSearchByNames_Playlist(dictSearch : dict) :
    charToFormat = f"""WHERE nomPlaylist LIKE '%{dictSearch["searchPlaylist"]}%'"""
    cr, res = executeRequest(_requetes["getAll_Playlist"].format(charToFormat))
    attr = ["numPlaylist", "nomPlaylist", "pseudonyme", "theme", "dateCreation"]
    attrToName(attr, "playlist")
    return res, attr
### ---------------------------------------------------------------------------------------------
### --- Requêtes de recherches triés en fonction des tables et des pages --- ### 
def reqSortedPrint(table : str, dictSearch : dict) :
    colAndDescChar = dictSearch["sortSelection"]
    if "ordreInverse" in dictSearch : colAndDescChar += " DESC"
    cr, res = executeRequest(_requetes["getSorted"].format(table, colAndDescChar))
    attr = getAttributesTab(table)
    attrToName(attr, table)
    return res, attr
def reqSortedPrint_Titre(dictSearch : dict) :
    charToFormat = "ORDER BY {}".format(dictSearch["sortSelection"])
    if "ordreInverse" in dictSearch : charToFormat += " DESC"
    cr, res = executeRequest(_requetes["getAll_Titre"].format(charToFormat))
    attr = ["numTitre", "nomTitre", "duree", "nomAlbum", "nomArtiste"]
    attrToName(attr, "titre")
    return res, attr
def reqSortedPrint_Album(dictSearch : dict) :
    charToFormat = "ORDER BY {}".format(dictSearch["sortSelection"])
    if "ordreInverse" in dictSearch : charToFormat += " DESC"
    cr, res = executeRequest(_requetes["getAll_Album"].format(charToFormat))
    attr = ["numAlbum", "nomAlbum", "nomArtiste", "classification", "dateSortie"]
    attrToName(attr, "album")
    return res, attr
def reqSortedPrint_Playlist(dictSearch : dict) :
    charToFormat = "ORDER BY {}".format(dictSearch["sortSelection"])
    if "ordreInverse" in dictSearch : charToFormat += " DESC"
    cr, res = executeRequest(_requetes["getAll_Playlist"].format(charToFormat))
    attr = ["numPlaylist", "nomPlaylist", "pseudonyme", "theme", "dateCreation"]
    attrToName(attr, "playlist")
    return res, attr
### ---------------------------------------------------------------------------------------------
### --- Requêtes de recherches par nom en fonction des tables et des pages --- ### 
def insertTitre(dictInsert : dict) :
    attrToInsert = ""
    valueToInsert = ""
    if dictInsert["titreName"] != "" : 
        attrToInsert += "nomTitre"
        valueToInsert += '"' + dictInsert["titreName"] + '"'
    if dictInsert["heures"] != "" and dictInsert["minutes"] != "" and dictInsert["secondes"] != "" :
        attrToInsert += ",duree"
        duree = f'{dictInsert["heures"]}:{dictInsert["minutes"]}:{dictInsert["secondes"]}'
        valueToInsert += ',"' + duree + '"'
    if dictInsert["numAlbum"] != "" : 
        attrToInsert += ",numAlbum"
        valueToInsert += ',"' + dictInsert["numAlbum"] + '"'
    if dictInsert["numArtiste"] != "" : 
        attrToInsert += ",numArtiste"
        valueToInsert += ',"' + dictInsert["numArtiste"] + '"'
    if len(valueToInsert.split('","')) == len(attrToInsert.split(",")) and valueToInsert != '' :
        executeRequest(_requetes["insertTitreWEB"].format(attrToInsert, valueToInsert))
def insertArtiste(dictInsert : dict) :
    attrToInsert = ""
    valueToInsert = ""
    if dictInsert["artisteName"] != "" : 
        attrToInsert += "nomArtiste"
        valueToInsert += '"' + dictInsert["artisteName"] + '"'
    if dictInsert["artisteDescri"] != "" : 
        attrToInsert += ",descripArt"
        valueToInsert += ',"' + dictInsert["artisteDescri"] + '"'
    if len(valueToInsert.split('","')) == len(attrToInsert.split(",")) and valueToInsert != '' :
        executeRequest(_requetes["insertArtisteWEB"].format(attrToInsert, valueToInsert))
def insertAlbum(dictInsert :dict) :
    attrToInsert = ""
    valueToInsert = ""
    if dictInsert["albumName"] != "" : 
        attrToInsert += "nomAlbum"
        valueToInsert += '"' + dictInsert["albumName"] + '"'
    if dictInsert["classification"] != "" : 
        attrToInsert += ",classification"
        valueToInsert += ',"' + dictInsert["classification"] + '"'
    if dictInsert["annees"] != "" and dictInsert["mois"] != "" and dictInsert["jours"] != "" :
        attrToInsert += ",dateSortie"
        if dictInsert["annees"] == "0000" or dictInsert["mois"] == "0" or dictInsert["jours"] == "0" : date = '0000-00-00'
        else : date = f'{dictInsert["annees"]}-{dictInsert["mois"]}-{dictInsert["jours"]}'
        valueToInsert += ',"' + date + '"'
    if dictInsert["numArtiste"] != "" : 
        attrToInsert += ",numArtiste"
        valueToInsert += ',"' + dictInsert["numArtiste"] + '"'
    if len(valueToInsert.split('","')) == len(attrToInsert.split(",")) and valueToInsert != '' :
        executeRequest(_requetes["insertAlbumWEB"].format(attrToInsert, valueToInsert))
def insertPlaylist(dictInsert :dict) :
    attrToInsert = ""
    valueToInsert = ""
    if dictInsert["playlistName"] != "" : 
        attrToInsert += "nomPlaylist"
        valueToInsert += '"' + dictInsert["playlistName"] + '"'
    if dictInsert["playlistTheme"] != "" : 
        attrToInsert += ",theme"
        valueToInsert += ',"' + dictInsert["playlistTheme"] + '"'
    if dictInsert["numCreateur"] != "" : 
        attrToInsert += ",numCreateur"
        valueToInsert += ',"' + dictInsert["numCreateur"] + '"'
    if len(valueToInsert.split('","')) == len(attrToInsert.split(",")) and valueToInsert != '' :
        executeRequest(_requetes["insertPlaylistWEB"].format(attrToInsert, valueToInsert))
def insertUser(dictInsert : dict) :
    attrToInsert = ""
    valueToInsert = ""
    if dictInsert["userPseudo"] != "" : 
        attrToInsert += "pseudonyme"
        valueToInsert += '"' + dictInsert["userPseudo"] + '"'
    if dictInsert["userDescri"] != "" : 
        attrToInsert += ",descripUtil"
        valueToInsert += ',"' + dictInsert["userDescri"] + '"'
    if len(valueToInsert.split('","')) == len(attrToInsert.split(",")) and valueToInsert != '' :
        executeRequest(_requetes["insertUtilisateurWEB"].format(attrToInsert, valueToInsert))
def insertRassembler(dictInsert : dict) :
    attrToInsert = ""
    valueToInsert = ""
    if dictInsert["numTitre"] != "" : 
        attrToInsert += "numTitre"
        valueToInsert += '"' + dictInsert["numTitre"] + '"'
    if dictInsert["numPlaylist"] != "" : 
        attrToInsert += ",numPlaylist"
        valueToInsert += ',"' + dictInsert["numPlaylist"] + '"'
    if len(valueToInsert.split('","')) == len(attrToInsert.split(",")) and valueToInsert != '' :
        executeRequest(_requetes["insertRassemblerWEB"].format(attrToInsert, valueToInsert))
### ---------------------------------------------------------------------------------------------
### --- Requêtes de récupération d'ID et de noms --- ### 
def reqDropMenu_User() :
    cr, res = executeRequest(_requetes["getDropMenuUser"])
    attr = ["numUtil", "pseudonyme"]
    attrToName(attr, "utilisateur")
    return res, attr
def reqDropMenu_Artiste() :
    cr, res = executeRequest(_requetes["getDropMenuArtiste"])
    attr = ["numArtiste", "nomArtiste"]
    attrToName(attr, "artiste")
    return res, attr
def reqDropMenu_Album() :
    cr, res = executeRequest(_requetes["getDropMenuAlbum"])
    attr = ["numAlbum", "nomAlbum"]
    attrToName(attr, "artiste")
    return res, attr
def reqDropMenu_Playlist() :
    cr, res = executeRequest(_requetes["getDropMenuPlaylist"])
    attr = ["numPlaylist", "nomPlaylist"]
    attrToName(attr, "playlist")
    return res, attr
def reqDropMenu_Titre() :
    cr, res = executeRequest(_requetes["getDropMenuTitre"])
    attr = ["numTitre", "nomTitre"]
    attrToName(attr, "titre")
    return res, attr
### ---------------------------------------------------------------------------------------------
### --- Requêtes de suppressions --- ### 
def reqDeleteTitre(dictParam : dict) :
    table, idTable, nameTable = "titre", "numTitre", "nomTitre"
    if dictParam["titreIdDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, idTable, dictParam["titreIdDelete"]))
        except ValueError as err : print(err)
    elif  dictParam["titreNameDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, nameTable, dictParam["titreNameDelete"]))
        except pymysql.err.ProgrammingError as err : print(err)  
def reqDeleteArtiste(dictParam : dict) :
    table, idTable, nameTable = "artiste", "numArtiste", "nomArtiste"
    if dictParam["artisteIdDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, idTable, dictParam["artisteIdDelete"]))
        except ValueError as err : print(err)
    elif  dictParam["artisteNameDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, nameTable, dictParam["artisteNameDelete"]))
        except pymysql.err.ProgrammingError as err : print(err)    
def reqDeleteAlbum(dictParam : dict) :
    table, idTable, nameTable = "album", "numAlbum", "nomAlbum"
    if dictParam["albumIdDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, idTable, dictParam["albumIdDelete"]))
        except ValueError as err : print(err)
    elif  dictParam["albumNameDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, nameTable, dictParam["albumNameDelete"]))
        except pymysql.err.ProgrammingError as err : print(err)    
def reqDeletePlaylist(dictParam : dict) :
    table, idTable, nameTable = "playlist", "numPlaylist", "nomPlaylist"
    if dictParam["playlistIdDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, idTable, dictParam["playlistIdDelete"]))
        except ValueError as err : print(err)
    elif  dictParam["playlistNameDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, nameTable, dictParam["playlistNameDelete"]))
        except pymysql.err.ProgrammingError as err : print(err)    
def reqDeleteInPlaylist(dictParam : dict) :
    idInPlaylist, nameInPlaylist = "titre.numTitre", "titre.nomTitre"
    if dictParam["intoPlaylistIdDelete"] != "" :
        try : cr, res = executeRequest(_requetes["deleteInPlaylist"].format(dictParam["numPlaylist"], idInPlaylist, dictParam["intoPlaylistIdDelete"]))
        except ValueError as err : print(err)
    elif  dictParam["intoPlaylistNameDelete"] != "" :
        try : cr, res = executeRequest(_requetes["deleteInPlaylist"].format(dictParam["numPlaylist"], nameInPlaylist, dictParam["intoPlaylistNameDelete"]))
        except pymysql.err.ProgrammingError as err : print(err)    
def reqDeleteUser(dictParam : dict) :
    table, idTable, nameTable = "utilisateur", "numUtil", "pseudonyme"
    if dictParam["userIdDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, idTable, dictParam["userIdDelete"]))
        except ValueError as err : print(err)
    elif  dictParam["userNameDelete"] != "" :
        try : cr, res = executeRequest(_requetes["delete"].format(table, nameTable, dictParam["userNameDelete"]))
        except pymysql.err.ProgrammingError as err : print(err)    
### ---------------------------------------------------------------------------------------------
### --- Requêtes de modification --- ### 
def getParamsToUpdate(table : str, dictParam : dict) :
    retour = ""
    if table == "artiste" :
        elemToFind = f"""numArtiste = '{dictParam["searchArtiste"]}'"""
        cr, res = executeRequest(_requetes["getByID"].format(table,elemToFind))
        retour = res[0]
    elif table == "album" :
        elemToFind = f"""numAlbum = '{dictParam["searchAlbum"]}'"""
        cr, res = executeRequest(_requetes["getByID"].format(table,elemToFind))
        retour = res[0]
    elif table == "titre" :
        elemToFind = f"""numTitre = '{dictParam["searchTitre"]}'"""
        cr, res = executeRequest(_requetes["getByID"].format(table,elemToFind))
        retour = res[0]
    elif table == "playlist" :
        elemToFind = f"""numPlaylist = '{dictParam["searchPlaylist"]}'"""
        cr, res = executeRequest(_requetes["getByID"].format(table,elemToFind))
        retour = res[0]
    elif table == "utilisateur" :
        elemToFind = f"""numUtil = '{dictParam["searchUser"]}'"""
        cr, res = executeRequest(_requetes["getByID"].format(table,elemToFind))
        retour = res[0]
    return retour

def updatingUserData(previousData : tuple, dictParam : dict) :
    newDataList = []
    if previousData[1] == dictParam["userPseudo"] or dictParam["userPseudo"] == "" : newDataList.append(previousData[1])
    else : newDataList.append(dictParam["userPseudo"])
    if previousData[2] == dictParam["userDescri"] or dictParam["userDescri"] == "" : newDataList.append(previousData[2])
    else : newDataList.append(dictParam["userDescri"])
    executeRequest(_requetes["updateUserByID"].format(newDataList[0], newDataList[1], previousData[0]))
def updatingArtisteData(previousData : tuple, dictParam : dict) :
    newDataList = []
    if previousData[1] == dictParam["artisteName"] or dictParam["artisteName"] == "" : newDataList.append(previousData[1])
    else : newDataList.append(dictParam["artisteName"])
    if previousData[2] == dictParam["artisteDescri"] or dictParam["artisteDescri"] == "" : newDataList.append(previousData[2])
    else : newDataList.append(dictParam["artisteDescri"])
    executeRequest(_requetes["updateArtisteByID"].format(newDataList[0], newDataList[1], previousData[0]))
def updatingAlbumData(previousData : tuple, dictParam : dict) :
    newDataList = []
    if previousData[1] == dictParam["albumName"] or dictParam["albumName"] == "" : newDataList.append(previousData[1])
    else : newDataList.append(dictParam["albumName"])
    if previousData[2] == dictParam["classification"] or dictParam["classification"] == "" : newDataList.append(previousData[2])
    else : newDataList.append(dictParam["classification"])
    newDate = f'{dictParam["annees"]}-{dictParam["mois"]}-{dictParam["jours"]}'
    if previousData[3] == newDate or newDate == "" : newDataList.append(previousData[3])
    else : newDataList.append(newDate)
    if previousData[4] == dictParam["numArtiste"] or dictParam["numArtiste"] == "" : newDataList.append(previousData[4])
    else : newDataList.append(dictParam["numArtiste"])
    executeRequest(_requetes["updateAlbumByID"].format(newDataList[0], newDataList[1], newDataList[2], newDataList[3], previousData[0]))
def updatingPlaylistData(previousData : tuple, dictParam : dict) :
    newDataList = []
    if previousData[1] == dictParam["playlistName"] or dictParam["playlistName"] == "" : newDataList.append(previousData[1])
    else : newDataList.append(dictParam["playlistName"])
    if previousData[2] == dictParam["playlistTheme"] or dictParam["playlistTheme"] == "" : newDataList.append(previousData[2])
    else : newDataList.append(dictParam["playlistTheme"])
    if previousData[4] == dictParam["numCreateur"] or dictParam["numCreateur"] == "" : newDataList.append(previousData[4])
    else : newDataList.append(dictParam["numCreateur"])
    executeRequest(_requetes["updatePlaylistByID"].format(newDataList[0], newDataList[1], newDataList[2], previousData[0]))
def updatingTitreData(previousData : tuple, dictParam : dict) :
    newDataList = []
    if previousData[1] == dictParam["titreName"] or dictParam["titreName"] == "" : newDataList.append(previousData[1])
    else : newDataList.append(dictParam["titreName"])
    newDuree = f'{dictParam["heures"]}:{dictParam["minutes"]}:{dictParam["secondes"]}'
    if previousData[2] == newDuree or newDuree == "" : newDataList.append(previousData[2])
    else : newDataList.append(newDuree)
    if previousData[3] == dictParam["numAlbum"] or dictParam["numAlbum"] == "" : newDataList.append(previousData[3])
    else : newDataList.append(dictParam["numAlbum"])
    if previousData[4] == dictParam["numArtiste"] or dictParam["numArtiste"] == "" : newDataList.append(previousData[4])
    else : newDataList.append(dictParam["numArtiste"])
    executeRequest(_requetes["updateTitreByID"].format(newDataList[0], newDataList[1], newDataList[2], newDataList[3], previousData[0]))
def AVoter(nb : str) :
    cr, res = executeRequest(_requetes["getMaxSondageId"])
    maxId = res[0][0]
    executeRequest(_requetes["updateSondage"].format(nb, nb, maxId))
def insertNewSondage(dictParam : dict) :
    # {'sondDropMenu_Pl1': '3', 'sondDropMenu_Pl2': '3', 'sondDropMenu_Pl3': '3'}
    verificationOK = ((dictParam["sondDropMenu_Pl1"] != dictParam["sondDropMenu_Pl2"]) and (dictParam["sondDropMenu_Pl1"] != dictParam["sondDropMenu_Pl3"]) \
        and (dictParam["sondDropMenu_Pl2"] != dictParam["sondDropMenu_Pl3"])) and \
        (dictParam["sondDropMenu_Pl1"] != '0' and dictParam["sondDropMenu_Pl2"] != '0' and dictParam["sondDropMenu_Pl3"] != '0')
    retour = False
    if verificationOK : 
        executeRequest(_requetes["insertSondage"].format(dictParam["sondDropMenu_Pl1"], dictParam["sondDropMenu_Pl2"], dictParam["sondDropMenu_Pl3"]))
        retour = True
    return retour

def reinitTables() :
    listTable = ["sondage", "rassembler", "titre", "playlist", "album", "artiste", "utilisateur"]
    for table in listTable :
        executeRequest(_requetes["dropTable"].format(table)) 
    _db, _cursor = dbConnect(True)
    creationTables(_cursor)
    creationRelations(_cursor)
    defautInsert(_cursor)
    _db.commit()

def reinitDataBase():
    createBaseMySQL()
    _db, _cursor = dbConnect(True)
    defautInsert(_cursor)
    _db.commit()

### ---------------------------------------------------------------------------------------------
### --- Construction de texte HTML pour affichage --- ### 
def constructHTML_dropMenu(attrRef : str, resLines : tuple) :
    charHTML = f'<select class="dropMenu" name="{attrRef}">\n'
    for element in resLines[0] :
        charHTML += f'<option value="{element[0]}">{element[1]}</option>'
    charHTML += "</select>"
    return charHTML
def constructHTML_tabAffichage(resLines : tuple) :
    charHTML = '<table>'
    charHTML +='<tr>'
    for index in range(len(resLines[1])) :
        charHTML +='<th>' + f"{resLines[1][index]}" + '</th>'
    charHTML += "</tr>\n"
    for line in range(len(resLines[0])):
        charHTML +='<tr>'
        for col in range(len(resLines[0][line])) :
            charHTML +='<td>' + str(resLines[0][line][col]) + '</td>'
        charHTML += "</tr>\n"
    charHTML += "</table>"
    return charHTML


def char_ModifiedDropMenu(attrRef : str, resLines : tuple, numToSelect) :
    charHTML = f'<select class="dropMenu" name="{attrRef}">\n'
    for element in resLines[0] :
        if element[0] == numToSelect : charHTML += f'<option value="{element[0]}" selected>{element[1]}</option>'
        else : charHTML += f'<option value="{element[0]}">{element[1]}</option>'
    charHTML += "</select>"
    return charHTML

def constructHTML_updateUser(tupleParams : tuple) :
    charHTML = """
        <form action="userGetUpdate">
            <h3 class="lowLineHeight">Nouvelles informations de l'utilisateur n°{} :<span class="fontSize16 lowLineHeight"> ne pas remplir un champs
                laissera à la donnée correspondante sa valeur d'origine</h3>
            <h4 class="leftMarge">Pseudonyme du nouvel utilisateur :</h4>
            <input type="search" name="userPseudo" placeholder="{} " class="insertPseudoBar" maxlength="75">
            <br>
            <div class="leftMarge decalageHaut">
                <label class="labelDescri lowLineHeight" for="userDescri">Description de l'utilisateur :<span class="fontSize16"> décrivez l'utilisateur
                    (vous peut-être), si vous le souhaitez (styles préférés ou détestés, caractéristiques, etc)</span></label>
                <textarea id="userDescri" name="userDescri" rows="5" cols="92" maxlength="250">{}</textarea>
            </div>

            <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Modifier</button>
        </form>
        """.format(tupleParams[0], tupleParams[1], tupleParams[2],)
    return charHTML

def constructHTML_updateArtiste(tupleParams : tuple) :
    charHTML = """
        <form action="artisteGetUpdate">
            <h4 class="lowLineHeight">Nouvelles informations de l'artiste ou du groupe n°{} :<span class="fontSize16 lowLineHeight"> ne pas remplir un champs laissera à la donnée correspondante sa valeur d'origine</span></h4>
            <h4 class="leftMarge">Nom de l'album, EP ou single :</h4>
            <input type="search" name="artisteName" placeholder="{} " class="insertPseudoBar" maxlength="75">
            <br>
            <div class="leftMarge decalageHaut">
                <label class="labelDescri lowLineHeight" for="artisteDescri">Description de l'artiste ou du groupe :<span class="fontSize16"> décrivez l'artiste ou le 
                    groupe si vous le connaissez un peu (genre, origine, inspiration, etc)</span></label>
                <textarea id="artisteDescri" name="artisteDescri" rows="5" cols="92" maxlength="250">{}</textarea>
            </div>
            <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Modifier</button>
        </form>
        """.format(tupleParams[0], tupleParams[1], tupleParams[2],)
    return charHTML

def char_PartFormUpdateAlbum(classification, date):
    # Mise en forme de la date
    try :
        annee, mois, jour = date.year, date.month, date.day
    except AttributeError as err :
        annee, mois, jour = "0000", "0", "0"
    
    # Menu déroulant de classification
    charToReturn = """<div class="insertAlbumBox">\n<h4 class="">Classification (album, EP ou single) :</h4>"""
    charToReturn +=  """<select id="classifBar" name="classification" class="dropMenu" style="margin-left: 10%;">"""
    if classification == "Album" : charToReturn += """<option value="Album" selected>Album</option>"""
    else : charToReturn += """<option value="Album">Album</option>"""
    if classification == "EP" : charToReturn += """<option value="EP" selected>EP</option>"""
    else : charToReturn += """<option value="EP">EP</option>"""
    if classification == "Single" : charToReturn += """<option value="Single" selected>Single</option>"""
    else : charToReturn += """<option value="Single">Single</option>"""
    charToReturn += """</select>\n</div>\n"""
    # Menu déroulant de date
    listeMois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
    charToReturn += """<div class="displayCol insertAlbumBox">\n<h4 class="">Date de sortie :</h4>\n<div>\n"""
    charToReturn += """<select class="dropMenuDate menuJour" name="jours">\n""" 
    yearBoolDone, monthBoolDone, dayBoolDone = False, False, False
    for index in range(32) :
        if index % 10 == 0 : charToReturn += "\n"
        if jour != "0" and index == jour : charToReturn += f"""<option value="{index}" selected>{index}</option>"""
        elif jour == "0" and not(dayBoolDone) : 
            charToReturn += f"""<option value="0" selected>0</option>"""
            dayBoolDone = True
        else : charToReturn += f"""<option value="{index}">{index}</option>"""
    charToReturn += """</select>\n"""
    charToReturn += """<select class="dropMenuDate menuMois" name="mois">\n"""
    for index in range(13) :
        if index == 5 or index == 9 : charToReturn += "\n"
        if mois != "0" and index == mois : charToReturn += f"""<option value="{index}" selected>{listeMois[index-1]}</option>"""
        elif mois == "0" and not(monthBoolDone) : 
            charToReturn += f"""<option value="0" selected>0</option>"""
            monthBoolDone = True
        else : charToReturn += f"""<option value="{index}">{listeMois[index-1]}</option>"""
    charToReturn += """</select>\n"""
    charToReturn += """<select class="dropMenuDate menuAnnee" name="annees">\n"""
    for index in range(2025, 1922, -1) :
        if index % 10 == 0 : charToReturn += "\n"
        if annee != "0000" and index == annee : charToReturn += f"""<option value="{index}" selected>{index}</option>"""
        elif annee == "0000" and not(yearBoolDone) : 
            charToReturn += f"""<option value="0000" selected>0</option>"""
            yearBoolDone = True
        else : charToReturn += f"""<option value="{index}">{index}</option>"""
    charToReturn += """</select>\n</div>\n</div>\n"""
    # Retour
    return charToReturn
def constructHTML_updateAlbum(tupleParams : tuple) :
    partChar_2 = char_PartFormUpdateAlbum(tupleParams[2], tupleParams[3])
    partChar_3 = char_ModifiedDropMenu("numArtiste", reqDropMenu_Artiste(), tupleParams[4])
    charHTML = """
        <form action="albumGetUpdate">
            <h4 class="lowLineHeight">Nouvelles informations de l'album, EP ou single n°{} :<span class="fontSize16 lowLineHeight"> ne pas remplir un champs 
            laissera à la donnée correspondante sa valeur d'origine</span></h4>
            <h4 class="leftMarge">Nom de l'album :</h4>
            <input type="search" name="albumName" placeholder="{} " class="insertPseudoBar" maxlength="75">
            <br>
            <div class="leftMarge displayRow" >
                {}
            </div>
            <div>
                <h4 class="leftMarge">Indiquez l'artiste ou le groupe auteur de cet album :</h4>
                <div class="leftMarge decalageBas">
                    <div class="leftMarge dropMenuAlbArtUser">
                        {}
                    </div>
                </div>
            </div>
            <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Modifier</button>
        </form>
        """.format(tupleParams[0], tupleParams[1], partChar_2, partChar_3)
    return charHTML

def constructHTML_updatePlaylist(tupleParams : tuple) :
    partChar = char_ModifiedDropMenu("numCreateur", reqDropMenu_User(), tupleParams[4])
    charHTML = """
        <form action="playlistGetUpdate">
            <h4 class="lowLineHeight">Nouvelles informations de la playlist n°{} :<span class="fontSize16 lowLineHeight"> ne pas remplir un champs 
            laissera à la donnée correspondante sa valeur d'origine</span></h4>
            <h4 class="leftMarge">Nom de la playlist :</h4>
            <input type="search" name="playlistName" placeholder="{} " class="insertPseudoBar" maxlength="75">
            <br>
            <div>
                <h4 class="leftMarge">Indiquez l'utilisateur créant cette playlist :</h4>
                <div class="leftMarge dropMenuAlbArtUser">
                    {}
                </div>
            </div>
            <div class="leftMarge lowLineHeight">
                <label class="labelDescri" for="playlistTheme">Décrivez la nouvelle playlist :<span class="fontSize16"> décrivez le thème de cette 
                        playlist, ses inspirations, etc.</span></label>
                <textarea id="playlistTheme" name="playlistTheme" rows="5" cols="63" maxlength="250">{}</textarea>
            </div>
            <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Modifier</button>
        </form>
    """.format(tupleParams[0], tupleParams[1], partChar, tupleParams[2])
    return charHTML

def char_PartFormUpdateTitre(duree):
    # Mise en forme de la duree
    totSec = duree.total_seconds()
    heure = totSec // 3600
    minute = (totSec - heure*3600) // 60
    sec = (totSec - heure*3600 - minute*60)
    # Menu déroulant de date
    charToReturn = """<div class="dureeSon">\n<div class="displayRow widthDuree">\n<h5 id="menuHour">Heures : </h5>"""
    charToReturn += """<select class="dropMenuTime" name="heures">""" 
    for index in range(12) :
        if index % 4 == 0 : charToReturn += "\n"
        if index <= 9 : numInValue = f'0{index}'
        else : numInValue = f'{index}'
        if index == heure : charToReturn += f"""<option value="{numInValue}" selected>{index}</option>"""
        else : charToReturn += f"""<option value="{numInValue}">{index}</option>"""
    charToReturn += """</select>\n</div>\n"""
    charToReturn += """<div class="displayRow widthDuree">\n<h5>Minutes : </h5>\n"""
    charToReturn += """<select class="dropMenuTime" name="minutes">"""
    for index in range(60) :
        if index % 10 == 0 : charToReturn += "\n"
        if index <= 9 : numInValue = f'0{index}'
        else : numInValue = f'{index}'
        if index == minute : charToReturn += f"""<option value="{numInValue}" selected>{index}</option>"""
        else : charToReturn += f"""<option value="{numInValue}">{index}</option>"""
    charToReturn += """</select>\n</div>\n"""
    charToReturn += """<div class="displayRow widthDuree">\n<h5>Secondes : </h5>\n"""
    charToReturn += """<select class="dropMenuTime" name="secondes">\n"""
    for index in range(60) :
        if index % 10 == 0 : charToReturn += "\n"
        if index <= 9 : numInValue = f'0{index}'
        else : numInValue = f'{index}'
        if index == sec : charToReturn += f"""<option value="{numInValue}" selected>{index}</option>"""
        else : charToReturn += f"""<option value="{numInValue}">{index}</option>"""
    charToReturn += """</select>\n</div>\n</div>"""
    # Retour
    return charToReturn
def constructHTML_updateTitre(tupleParams : tuple) :
    partChar_duree = char_PartFormUpdateTitre(tupleParams[2])
    partChar_menuAlb = char_ModifiedDropMenu("numAlbum", reqDropMenu_Album(), tupleParams[3])
    partChar_menuArt = char_ModifiedDropMenu("numArtiste", reqDropMenu_Artiste(), tupleParams[4])
    charHTML = """
    <form action="titreGetUpdate">
        <h4>Nouvelles informations du titre n°{} :<span class="fontSize16"> ne pas remplir un champs laissera à la donnée correspondante sa valeur d'origine</span></h4>
        <h4 class="leftMarge">Nom du titre :</h4>
        <input type="search" name="titreName" placeholder="{} " class="insertPseudoBar" maxlength="75">
        <br>
        <h4 class="leftMarge">Durée du titre :<span class="fontSize16"> établir à 0 si vous ne connaissez pas la durée du morceau</span></h4>
        {}
        <div class="displayRow leftMarge">
            <div class="displayCol artistSelectCol">
                <h4>Artiste auteur de ce titre :</h4>
                <div class="thickDropMenu">
                    {}
                </div>
            </div>
            <div class="displayCol albumSelectCol">
                <h4>Album auquel appartient ce titre :</h4>
                <div class="thickDropMenu">
                    {}
                </div>
            </div>
        </div>
        <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Modifier</button>
    </form>
    """.format(tupleParams[0], tupleParams[1], partChar_duree, partChar_menuArt, partChar_menuAlb)
    return charHTML

def constructHTML_actualSondage():
    cr, res = executeRequest(_requetes["getActualSondage"])
    try :
        dataSondage = res[0]
    except IndexError as err :
        charActSond = """
        <div class="centeredText">
            <h3>AUCUN SONDAGE N'A ÉTÉ PROGRAMMÉ POUR LE MOMENT</h3>
            <h4>Contacter l'administrateur du site pour plus d'information</h4> 
        </div>
        """
    else :
        # Construstion des variables 
        # Numéros des playlists
        listeNumPl = []
        for index in range(1,6,2) :
            if int(dataSondage[index]) <= 9 : listeNumPl.append(f"0{dataSondage[index]}")
            else : listeNumPl.append(f"{dataSondage[index]}")
        # Noms des playlists
        listeNamePl = []
        for index in range(len(listeNumPl)) :
            cr, res = executeRequest(_requetes["getPlaylistNamePlById"].format(listeNumPl[index]))
            listeNamePl.append(res[0][0])
        # Créateurs des playlists
        listeCreateurPl = []
        for index in range(len(listeNumPl)) :
            cr, res = executeRequest(_requetes["getPlaylistNameCreaById"].format(listeNumPl[index]))
            listeCreateurPl.append(res[0][0])
        # Dates de création des playlists
        listeDateCreationPl = []
        for index in range(len(listeNumPl)) :
            cr, res = executeRequest(_requetes["getPlaylistDateCreateById"].format(listeNumPl[index]))
            annee, mois, jour = res[0][0].year, res[0][0].month, res[0][0].day
            listeDateCreationPl.append(f"{annee}-{mois}-{jour}")
        # Thèmes des playlists
        listeThemePl = []
        for index in range(len(listeNumPl)) :
            cr, res = executeRequest(_requetes["getPlaylistThemeById"].format(listeNumPl[index]))
            listeThemePl.append(res[0][0])
        # Nombres de votes
        listeNbVote = []
        for index in range(2,7,2) :
            listeNbVote.append(f"{dataSondage[index]}")
        # Construction de la chaîne de caractère finale
        charActSond = f"""
        <div>
            <h2>Sondage de la semaine - sondage n°{dataSondage[0]} : </h2>
        </div>
        <div class="displayRow affCard">
            <div class="playlistCard card_1">
                <div class="displayCol">
                    <div class="minHeightSond">
                        <h2 class="fontAudiowide fontSize24 w3-center">Playlist n°{listeNumPl[0]} :<br>{listeNamePl[0]}</h2>
                        <h3 class="textToLeft fontSize20">Créateur : {listeCreateurPl[0]}</h3>
                        <h4 class="textToLeft fontSize16 sondCreationDate"><u>Crée le :</u> {listeDateCreationPl[0]}</h4>
                        <h4 class="textToLeft fontSize16 sondTheme"><u>Thème :</u> {listeThemePl[0]}</h4>
                    </div>
                    <div class="buttonVote">
                        <form action="voter1">
                            <button type="submit" class="submitButton buttonHoverCustomGreen standardButtonPl">Je vote</button>
                        </form>
                    </div>
                    <hr>
                    <h2 class="fontAudiowide fontSize24 w3-center sondNbVote">Nombre de votes<br>actuels : {listeNbVote[0]}</h2>
                </div>
            </div>
            <div class="playlistCard card_2">
                <div class="displayCol">
                    <div class="minHeightSond">
                        <h2 class="fontAudiowide fontSize24 w3-center">Playlist n°{listeNumPl[1]} :<br>{listeNamePl[1]}</h2>
                        <h3 class="textToLeft fontSize20">Créateur : {listeCreateurPl[1]}</h3>
                        <h4 class="textToLeft fontSize16 sondCreationDate"><u>Crée le :</u> {listeDateCreationPl[1]}</h4>
                        <h4 class="textToLeft fontSize16 sondTheme"><u>Thème :</u> {listeThemePl[1]}</h4>
                    </div>
                    <div class="buttonVote">
                        <form action="voter2">
                            <button type="submit" class="submitButton buttonHoverCustomGreen standardButtonPl">Je vote</button>
                        </form>
                    </div>
                    <hr>
                    <h2 class="fontAudiowide fontSize24 w3-center sondNbVote">Nombre de votes<br>actuels : {listeNbVote[1]}</h2>
                </div>
            </div>
            <div class="playlistCard card_3">
                <div class="displayCol">
                    <div class="minHeightSond">
                        <h2 class="fontAudiowide fontSize24 w3-center">Playlist n°{listeNumPl[2]} :<br>{listeNamePl[2]}</h2>
                        <h3 class="textToLeft fontSize20">Créateur : {listeCreateurPl[2]}</h3>
                        <h4 class="textToLeft fontSize16 sondCreationDate"><u>Crée le :</u> {listeDateCreationPl[2]}</h4>
                        <h4 class="textToLeft fontSize16 sondTheme"><u>Thème :</u> {listeThemePl[2]}</h4>
                    </div>
                    <div class="buttonVote">
                        <form action="voter3">
                            <button type="submit" class="submitButton buttonHoverCustomGreen standardButtonPl">Je vote</button>
                        </form>
                    </div>
                    <hr>
                    <h2 class="fontAudiowide fontSize24 w3-center sondNbVote">Nombre de votes<br>actuels : {listeNbVote[2]}</h2>
                </div>
            </div>
        </div>
        """
    return charActSond

def constructHTML_dropMenu_Modified(nbPlaylist : str, resLines : tuple) :
    charHTML = f'  <select class="dropMenu" name="sondDropMenu_Pl{nbPlaylist}">\n<option value="0">Playlist n°{nbPlaylist}</option>\n'
    for element in resLines[0] :
        charHTML += f'              <option value="{element[0]}">{element[1]}</option>\n'
    charHTML += "           </select>"
    return charHTML
def constructHTML_MajSondage() :
    charDropMenuPl1 = constructHTML_dropMenu_Modified("1", reqDropMenu_Playlist())
    charDropMenuPl2 = constructHTML_dropMenu_Modified("2", reqDropMenu_Playlist())
    charDropMenuPl3 = constructHTML_dropMenu_Modified("3", reqDropMenu_Playlist())
    charDropMenuMajSond = f"""
    <div class="sondDropMenu">
        {charDropMenuPl1}
    </div>
    <div class="sondDropMenu">
        {charDropMenuPl2}
    </div>
    <div class="sondDropMenu">
        {charDropMenuPl3}
    </div>
    """
    return charDropMenuMajSond
    
def constructHTML_pastSondage():
    cr, res = executeRequest(_requetes["getPastSondage"])
    charPastSond = ""
    if len(res) != 0 :
        for index in range(len(res)):
            # Isolation des variables souhaitées
            currentSondage = res[index]
            if currentSondage[2] == max([currentSondage[2], currentSondage[4], currentSondage[6]]) : 
                numSondage = f"{currentSondage[0]}"
                cr, result = executeRequest(_requetes["getPlaylistNamePlById"].format(currentSondage[1]))
                nomPl = result[0][0]
                BestPl = (numSondage, nomPl, currentSondage[2])
            elif currentSondage[4] == max([currentSondage[2], currentSondage[4], currentSondage[6]]) : 
                numSondage = f"{currentSondage[0]}"
                cr, result = executeRequest(_requetes["getPlaylistNamePlById"].format(currentSondage[3]))
                nomPl = result[0][0]
                BestPl = (numSondage, nomPl, currentSondage[4])
            elif currentSondage[6] == max([currentSondage[2], currentSondage[4], currentSondage[6]]) : 
                numSondage = f"{currentSondage[0]}"
                cr, result = executeRequest(_requetes["getPlaylistNamePlById"].format(currentSondage[5]))
                nomPl = result[0][0]
                BestPl = (numSondage, nomPl, currentSondage[6])
            # Construction de la chaîne de caractère
            if (index+1) % 2 == 1 :
                charPastSond += '\n<div class="displayRow">\n'
                charPastSond += f"""    <div class="vainqueurCentered vainqueurPasse vainqueurGauche"><h4>Sondage n°{BestPl[0]}<br><span class="fontAudiowide">{BestPl[1]}</span><br><span class="fontSize20">Votée {BestPl[2]} fois</span></h4></div>\n"""
            elif (index+1) % 2 == 0 :
                charPastSond += f"""    <div class="vainqueurCentered vainqueurPasse vainqueurDroite"><h4>Sondage n°{BestPl[0]}<br><span class="fontAudiowide">{BestPl[1]}</span><br><span class="fontSize20">Votée {BestPl[2]} fois</span></h4></div>\n"""
                charPastSond += '</div>'
            if len(res) % 2 == 1 and index == len(res)-1 : 
                charPastSond += '</div>\n'
    else :
        charPastSond = """<h4 class="centeredText">Aucun sondage passé n'est présent dans la base de données</h4>"""
    return charPastSond
    
def initBase() :
    """ Teste la connexion à la base sur le serveur hôte, et propose sa création si nécessaire

    Raises:
        e: erreur d'opération sur la base
        e: erreur de refus de construction de la base
    """
    try :
        tryConnect()
    except pymysql.err.OperationalError or ConnectionRefusedError as e:
        if '1044' in str(e) :
            print ("Vérifiez les paramètres de connexion")
            raise e
        elif '1049' in str(e) :
            choix = input("La base 'musique' n'existe pas. Voulez-vouz créer cette base de données ? (Y/N) : ")
            if choix not in 'yYOo' : raise e
            else : 
                createBaseMySQL()
                choix_2 = input("Intégrer les données minimales de la base (optionel) ? (Y/N) : ")
                if choix_2 in 'yYOo' or choix_2 == "" :
                    _db, _cursor = dbConnect(True)
                    defautInsert(_cursor)
                    _db.commit()
        elif '2003' in str(e) :
            print ("!!! -- Vérifiez le bon fonctionnement de vos services de bases de données (MariaDB, MySQL, etc) -- !!! \n ")
            raise e

if __name__ == "__main__" :
    print()