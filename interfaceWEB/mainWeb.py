# Axel MOURILLON - SAE 2.3 
# -*- coding: utf-8 -*-

# Ajout du répertoire parent à la recherche de module Python
import sys, os, os.path, cherrypy, pymysql
sys.path.append('../')
# Importations
from mako.lookup import TemplateLookup
from mainCLI import initBase
from SQLRequestRef import _requetes
from databaseUtils import fillFromCSV, saveInCSV, reqNamePlaylist, getParamsToUpdate, reinitTables, reinitDataBase, \
    constructHTML_tabAffichage, constructHTML_dropMenu, executeRequest,\
    constructHTML_updateUser, constructHTML_updateArtiste, constructHTML_updateAlbum, constructHTML_updatePlaylist, constructHTML_updateTitre,\
    reqAllInTable, reqAllInTable_Titre, reqAllInTable_Album, reqAllInTable_Playlist, reqAllTitreInPlaylist,\
    reqSearchByNames, reqSearchByNames_Titre, reqSearchByNames_Album, reqSearchByNames_Playlist, \
    reqSortedPrint, reqSortedPrint_Titre, reqSortedPrint_Album, reqSortedPrint_Playlist, \
    reqDropMenu_User, reqDropMenu_Artiste, reqDropMenu_Album, reqDropMenu_Titre, reqDropMenu_Playlist,\
    insertUser, insertPlaylist, insertArtiste, insertAlbum, insertTitre, insertRassembler, \
    reqDeleteTitre, reqDeleteAlbum, reqDeleteArtiste, reqDeletePlaylist, reqDeleteInPlaylist, reqDeleteUser, \
    updatingUserData, updatingArtisteData, updatingAlbumData, updatingPlaylistData, updatingTitreData,\
    constructHTML_actualSondage, constructHTML_pastSondage, AVoter, constructHTML_MajSondage, insertNewSondage
    
# Définition d'un lookup mako
lookup = TemplateLookup(directories=['web/template'], input_encoding='utf-8', module_directory='web/template/makoTemp')

class InterfaceWebMusique(object):
    ### --- Constructeur --- ###
    def __init__(self) :
        self.charHTML_tabAffichage = ""
        self.previousData = ""
        # Menus déroulants
        self.charHTML_dropMenu_alb = constructHTML_dropMenu("numAlbum", reqDropMenu_Album())
        self.charHTML_dropMenu_art = constructHTML_dropMenu("numArtiste", reqDropMenu_Artiste())
        self.charHTML_dropMenu_user = constructHTML_dropMenu("numCreateur", reqDropMenu_User())
        self.charHTML_dropMenu_tit = constructHTML_dropMenu("numTitre", reqDropMenu_Titre())
        self.charHTML_dropMenu_play = constructHTML_dropMenu("numPlaylist", reqDropMenu_Playlist())
        # Formulaire de modifiaction 
        self.charHTML_updateTitre = ""
        self.charHTML_updateArtiste = ""
        self.charHTML_updateAlbum = ""
        self.charHTML_updatePlaylist = ""
        self.charHTML_updateUser = ""
        # IDs max de chaque table 
        self.idMaxArtiste =  executeRequest(_requetes["getMaxIDArtiste"])[1][0][0]
        self.idMaxAlbum = executeRequest(_requetes["getMaxIDAlbum"])[1][0][0]
        self.idMaxTitre = executeRequest(_requetes["getMaxIDTitre"])[1][0][0]
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        self.idMaxUser = executeRequest(_requetes["getMaxIDUser"])[1][0][0]
        
    ############################################################################################################
    ############################################################################################################
    ### --- Page d'accueil --- ###
    @cherrypy.expose
    def index(self) :
        template = lookup.get_template("index.html")
        return template.render()
    @cherrypy.expose
    def importCSV(self) :
        template = lookup.get_template("index.html")
        fillFromCSV()
        return template.render()
    @cherrypy.expose
    def exportCSV(self) :
        template = lookup.get_template("index.html")
        saveInCSV()
        return template.render()
    @cherrypy.expose
    def reinitialisationTables(self) :
        template = lookup.get_template("index.html")
        reinitTables()
        self.__init__()
        return template.render()
    @cherrypy.expose
    def reinitialisationDataBase(self) :
        template = lookup.get_template("index.html")
        reinitDataBase()
        self.__init__()
        return template.render()
    
    ############################################################################################################
    ############################################################################################################
    ### --- Page des titres --- ###
    @cherrypy.expose
    def titre(self) :
        template = lookup.get_template("titre.html")
        self.charHTML_dropMenu_alb = constructHTML_dropMenu("numAlbum", reqDropMenu_Album())
        self.charHTML_dropMenu_art = constructHTML_dropMenu("numArtiste", reqDropMenu_Artiste())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Titre())
        self.charHTML_updateTitre = ""
        self.previousData = ""
        self.idMaxTitre = executeRequest(_requetes["getMaxIDTitre"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_alb=self.charHTML_dropMenu_alb, \
            charHTML_dropMenu_art=self.charHTML_dropMenu_art, charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, \
            charHTML_dropMenu_play=self.charHTML_dropMenu_play, charHTML_updateTitre=self.charHTML_updateTitre, \
            idMaxTitre=self.idMaxTitre
        )
    # Bar de recherche
    @cherrypy.expose
    def barSearchTitre(self, **params) :
        template = lookup.get_template("titre.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSearchByNames_Titre(params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_alb=self.charHTML_dropMenu_alb, \
            charHTML_dropMenu_art=self.charHTML_dropMenu_art, charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, \
            charHTML_dropMenu_play=self.charHTML_dropMenu_play, charHTML_updateTitre=self.charHTML_updateTitre, \
            idMaxTitre=self.idMaxTitre
        )
    # Trie de la recherche
    @cherrypy.expose
    def sortSearchTitre(self, **params) :
        template = lookup.get_template("titre.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSortedPrint_Titre(params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_alb=self.charHTML_dropMenu_alb, \
            charHTML_dropMenu_art=self.charHTML_dropMenu_art, charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, \
            charHTML_dropMenu_play=self.charHTML_dropMenu_play, charHTML_updateTitre=self.charHTML_updateTitre, \
            idMaxTitre=self.idMaxTitre
        )
    # Insertion d'un titre
    @cherrypy.expose
    def insertIntoTitre(self, **params) :
        template = lookup.get_template("titre.html")
        insertTitre(params)
        self.charHTML_dropMenu_tit = constructHTML_dropMenu("numTitre", reqDropMenu_Titre())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Titre())
        self.idMaxTitre = executeRequest(_requetes["getMaxIDTitre"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_alb=self.charHTML_dropMenu_alb, \
            charHTML_dropMenu_art=self.charHTML_dropMenu_art, charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, \
            charHTML_dropMenu_play=self.charHTML_dropMenu_play, charHTML_updateTitre=self.charHTML_updateTitre, \
            idMaxTitre=self.idMaxTitre
        )
    # Suppression d'un titre
    @cherrypy.expose
    def deleteTitre(self, **params) :
        template = lookup.get_template("titre.html")
        reqDeleteTitre(params)
        self.charHTML_dropMenu_tit = constructHTML_dropMenu("numTitre", reqDropMenu_Titre())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Titre())
        self.idMaxTitre = executeRequest(_requetes["getMaxIDTitre"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_alb=self.charHTML_dropMenu_alb, \
            charHTML_dropMenu_art=self.charHTML_dropMenu_art, charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, \
            charHTML_dropMenu_play=self.charHTML_dropMenu_play, charHTML_updateTitre=self.charHTML_updateTitre, \
            idMaxTitre=self.idMaxTitre
        )
    # Mise à jour d'un titre
    @cherrypy.expose
    def initiateUpdateTitre(self, **params) :
        template = lookup.get_template("titre.html")
        self.previousData = getParamsToUpdate("titre", params)
        self.charHTML_updateTitre = constructHTML_updateTitre(self.previousData)
        self.idMaxTitre = executeRequest(_requetes["getMaxIDTitre"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_alb=self.charHTML_dropMenu_alb, \
            charHTML_dropMenu_art=self.charHTML_dropMenu_art, charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, \
            charHTML_dropMenu_play=self.charHTML_dropMenu_play, charHTML_updateTitre=self.charHTML_updateTitre, \
            idMaxTitre=self.idMaxTitre
        )
    @cherrypy.expose
    def titreGetUpdate(self, **params) :
        template = lookup.get_template("titre.html")
        updatingTitreData(self.previousData, params)
        self.charHTML_updateTitre = ""
        self.previousData = ""
        self.charHTML_dropMenu_tit = constructHTML_dropMenu("numTitre", reqDropMenu_Titre())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Titre())
        self.idMaxTitre = executeRequest(_requetes["getMaxIDTitre"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_alb=self.charHTML_dropMenu_alb, \
            charHTML_dropMenu_art=self.charHTML_dropMenu_art, charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, \
            charHTML_dropMenu_play=self.charHTML_dropMenu_play, charHTML_updateTitre=self.charHTML_updateTitre, \
            idMaxTitre=self.idMaxTitre
        )
    
    ############################################################################################################
    ############################################################################################################
    ### --- Page des artistes --- ###
    @cherrypy.expose
    def artiste(self) :
        template = lookup.get_template("artiste.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("artiste"))
        self.charHTML_updateArtiste = ""
        self.previousData = ""
        self.idMaxArtiste =  executeRequest(_requetes["getMaxIDArtiste"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateArtiste=self.charHTML_updateArtiste, \
            idMaxArtiste=self.idMaxArtiste
            )
    # Bar de recherche
    @cherrypy.expose
    def barSearchArtiste(self, **params) :
        template = lookup.get_template("artiste.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSearchByNames("artiste", params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateArtiste=self.charHTML_updateArtiste, \
            idMaxArtiste=self.idMaxArtiste
            )
    # Trie de la recherche
    @cherrypy.expose
    def sortSearchArtiste(self, **params) :
        template = lookup.get_template("artiste.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSortedPrint("artiste", params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateArtiste=self.charHTML_updateArtiste, \
            idMaxArtiste=self.idMaxArtiste
            )
    # Insertion d'un artiste
    @cherrypy.expose
    def insertIntoArtiste(self, **params) :
        template = lookup.get_template("artiste.html")
        insertArtiste(params)
        self.charHTML_dropMenu_art = constructHTML_dropMenu("numArtiste", reqDropMenu_Artiste())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("artiste"))
        self.idMaxArtiste =  executeRequest(_requetes["getMaxIDArtiste"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateArtiste=self.charHTML_updateArtiste, \
            idMaxArtiste=self.idMaxArtiste
            )
    # Suppression d'un artiste
    @cherrypy.expose
    def deleteArtiste(self, **params) :
        template = lookup.get_template("artiste.html")
        reqDeleteArtiste(params)
        self.charHTML_dropMenu_art = constructHTML_dropMenu("numArtiste", reqDropMenu_Artiste())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("artiste"))
        self.idMaxArtiste =  executeRequest(_requetes["getMaxIDArtiste"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateArtiste=self.charHTML_updateArtiste, \
            idMaxArtiste=self.idMaxArtiste
            )
    # Mise à jour d'un artiste
    @cherrypy.expose
    def initiateUpdateArtiste(self, **params) :
        template = lookup.get_template("artiste.html")
        self.previousData = getParamsToUpdate("artiste", params)
        self.charHTML_updateArtiste = constructHTML_updateArtiste(self.previousData)
        self.idMaxArtiste =  executeRequest(_requetes["getMaxIDArtiste"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateArtiste=self.charHTML_updateArtiste, \
            idMaxArtiste=self.idMaxArtiste
            )
    @cherrypy.expose
    def artisteGetUpdate(self, **params) :
        template = lookup.get_template("artiste.html")
        updatingArtisteData(self.previousData, params)
        self.charHTML_updateArtiste = ""
        self.previousData = ""
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("artiste"))
        self.idMaxArtiste =  executeRequest(_requetes["getMaxIDArtiste"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateArtiste=self.charHTML_updateArtiste, \
            idMaxArtiste=self.idMaxArtiste
            )
    
    ############################################################################################################
    ############################################################################################################
    ### --- Page des albums --- ###
    @cherrypy.expose
    def album(self) :
        template = lookup.get_template("album.html")
        self.charHTML_dropMenu_art = constructHTML_dropMenu("numArtiste", reqDropMenu_Artiste())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Album())
        self.charHTML_updateAlbum = ""
        self.previousData = ""
        self.idMaxAlbum = executeRequest(_requetes["getMaxIDAlbum"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_art=self.charHTML_dropMenu_art, \
            charHTML_updateAlbum=self.charHTML_updateAlbum, idMaxAlbum=self.idMaxAlbum
            )
    # Bar de recherche
    @cherrypy.expose
    def barSearchAlbum(self, **params) :
        template = lookup.get_template("album.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSearchByNames_Album(params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_art=self.charHTML_dropMenu_art, \
            charHTML_updateAlbum=self.charHTML_updateAlbum, idMaxAlbum=self.idMaxAlbum
            )    # Trie de la recherche
    @cherrypy.expose
    def sortSearchAlbum(self, **params) :
        template = lookup.get_template("album.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSortedPrint_Album(params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_art=self.charHTML_dropMenu_art, \
            charHTML_updateAlbum=self.charHTML_updateAlbum, idMaxAlbum=self.idMaxAlbum
            )
    # Insertion d'un album
    @cherrypy.expose
    def insertIntoAlbum(self, **params) :
        template = lookup.get_template("album.html")
        insertAlbum(params)
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Album())
        self.idMaxAlbum = executeRequest(_requetes["getMaxIDAlbum"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_art=self.charHTML_dropMenu_art, \
            charHTML_updateAlbum=self.charHTML_updateAlbum, idMaxAlbum=self.idMaxAlbum
            )
    @cherrypy.expose
    def deleteAlbum(self, **params) :
        template = lookup.get_template("album.html")
        reqDeleteAlbum(params)
        self.charHTML_dropMenu_alb = constructHTML_dropMenu("numAlbum", reqDropMenu_Album())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Album())
        self.idMaxAlbum = executeRequest(_requetes["getMaxIDAlbum"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_art=self.charHTML_dropMenu_art, \
            charHTML_updateAlbum=self.charHTML_updateAlbum, idMaxAlbum=self.idMaxAlbum
            )
    # Mise à jour d'un album
    @cherrypy.expose
    def initiateUpdateAlbum(self, **params) :
        template = lookup.get_template("album.html")
        self.previousData = getParamsToUpdate("album", params)
        self.charHTML_updateAlbum = constructHTML_updateAlbum(self.previousData)
        self.idMaxAlbum = executeRequest(_requetes["getMaxIDAlbum"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_art=self.charHTML_dropMenu_art, \
            charHTML_updateAlbum=self.charHTML_updateAlbum, idMaxAlbum=self.idMaxAlbum
            )
    @cherrypy.expose
    def albumGetUpdate(self, **params) :
        template = lookup.get_template("album.html")
        updatingAlbumData(self.previousData, params)
        self.charHTML_updateAlbum = ""
        self.previousData = ""
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Album())
        self.charHTML_dropMenu_alb = constructHTML_dropMenu("numAlbum", reqDropMenu_Album())
        self.idMaxAlbum = executeRequest(_requetes["getMaxIDAlbum"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_art=self.charHTML_dropMenu_art, \
            charHTML_updateAlbum=self.charHTML_updateAlbum, idMaxAlbum=self.idMaxAlbum
            )
    
    ############################################################################################################
    ############################################################################################################
    ### --- Page des playlists --- ###
    @cherrypy.expose
    def playlist(self) :
        template = lookup.get_template("playlist.html")
        self.charHTML_dropMenu_user = constructHTML_dropMenu("numCreateur", reqDropMenu_User())
        self.charHTML_dropMenu_tit = constructHTML_dropMenu("numTitre", reqDropMenu_Titre())
        self.charHTML_dropMenu_play = constructHTML_dropMenu("numPlaylist", reqDropMenu_Playlist())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Playlist())
        self.charHTML_updatePlaylist = ""
        self.previousData = ""
        self.messErr = ""
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )    
    # Bar de recherche
    @cherrypy.expose
    def barSearchPlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        self.messErr = ""
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSearchByNames_Playlist(params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )    
    # Trie de la recherche
    @cherrypy.expose
    def sortSearchPlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        self.messErr = ""
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSortedPrint_Playlist(params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )    
    # Insertion d'une playlist
    @cherrypy.expose
    def insertIntoPlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        self.messErr = ""
        insertPlaylist(params)
        self.charHTML_dropMenu_play = constructHTML_dropMenu("numPlaylist", reqDropMenu_Playlist())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Playlist())
        self.charHTML_DropMenusMajSond = constructHTML_MajSondage()
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )
    # Suppression d'une playlist
    @cherrypy.expose
    def deletePlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        self.messErr = ""
        reqDeletePlaylist(params)
        self.charHTML_dropMenu_play = constructHTML_dropMenu("numPlaylist", reqDropMenu_Playlist())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Playlist())
        self.charHTML_DropMenusMajSond = constructHTML_MajSondage()
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )
    # Mise à jour d'une playlist
    @cherrypy.expose
    def initiateUpdatePlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        self.messErr = ""
        self.previousData = getParamsToUpdate("playlist", params)
        self.charHTML_updatePlaylist = constructHTML_updatePlaylist(self.previousData)
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )  
    @cherrypy.expose
    def playlistGetUpdate(self, **params) :
        template = lookup.get_template("playlist.html")
        updatingPlaylistData(self.previousData, params)
        self.charHTML_updatePlaylist = ""
        self.previousData = ""
        self.messErr = ""
        self.charHTML_dropMenu_play = constructHTML_dropMenu("numPlaylist", reqDropMenu_Playlist())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable_Playlist())
        self.charHTML_DropMenusMajSond = constructHTML_MajSondage()
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )
    ############################################################################################################
    ## ----- Contenu de playlists ----- ##
    # Afficher les titres d'une playlist
    @cherrypy.expose
    def affTitreInPlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        self.messErr = ""
        nomPlaylist = reqNamePlaylist(params["numPlaylist"])
        self.charHTML_tabAffichage = f'<h4 class="tabulation">Contenu de la playlist "{nomPlaylist}" : </h4>' + constructHTML_tabAffichage(reqAllTitreInPlaylist(params["numPlaylist"]))
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )
    # Insertion d'un titre à une playlist
    @cherrypy.expose
    def insertTitreInPlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        try :
            insertRassembler(params)
        except pymysql.err.IntegrityError as err :
            self.messErr = "Ce titre est déjà dans cette playlist"
        else : 
            self.messErr = ""
        nomPlaylist = reqNamePlaylist(params["numPlaylist"])
        self.charHTML_tabAffichage = f'<h4 class="tabulation">Affichage de la playlist "{nomPlaylist}" : </h4>' + constructHTML_tabAffichage(reqAllTitreInPlaylist(params["numPlaylist"]))
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )
    # Suppression dans une playlist
    @cherrypy.expose
    def deleteIntoPlaylist(self, **params) :
        template = lookup.get_template("playlist.html")
        self.messErr = ""
        reqDeleteInPlaylist(params)
        nomPlaylist = reqNamePlaylist(params["numPlaylist"])
        self.charHTML_tabAffichage = f'<h4 class="tabulation">Affichage de la playlist "{nomPlaylist}" : </h4>' + constructHTML_tabAffichage(reqAllTitreInPlaylist(params["numPlaylist"]))
        self.idMaxPlaylist = executeRequest(_requetes["getMaxIDPlaylist"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_dropMenu_user=self.charHTML_dropMenu_user, \
            charHTML_dropMenu_tit=self.charHTML_dropMenu_tit, charHTML_dropMenu_play=self.charHTML_dropMenu_play, \
            charHTML_updatePlaylist=self.charHTML_updatePlaylist, messErr=self.messErr, idMaxPlaylist=self.idMaxPlaylist
        )

    ############################################################################################################
    ############################################################################################################
    ### --- Page des utilisateurs --- ###
    # Page par défaut
    @cherrypy.expose
    def utilisateur(self) :
        template = lookup.get_template("utilisateur.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("utilisateur"))
        self.charHTML_updateUser = ""
        self.previousData = ""
        self.idMaxUser = executeRequest(_requetes["getMaxIDUser"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateUser=self.charHTML_updateUser, idMaxUser=self.idMaxUser
            )
    # Bar de recherche
    @cherrypy.expose
    def barSearchUser(self, **params) :
        template = lookup.get_template("utilisateur.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSearchByNames("utilisateur", params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateUser=self.charHTML_updateUser, idMaxUser=self.idMaxUser
            )
    # Trie de la recherche
    @cherrypy.expose
    def sortSearchUser(self, **params) :
        template = lookup.get_template("utilisateur.html")
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqSortedPrint("utilisateur", params))
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateUser=self.charHTML_updateUser, idMaxUser=self.idMaxUser
            )
    # Insertion d'un utilisateur
    @cherrypy.expose
    def insertIntoUser(self, **params) :
        template = lookup.get_template("utilisateur.html")
        insertUser(params)
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("utilisateur"))
        self.idMaxUser = executeRequest(_requetes["getMaxIDUser"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateUser=self.charHTML_updateUser, idMaxUser=self.idMaxUser
            )
    # Suppression d'un utilisateur
    @cherrypy.expose
    def deleteUser(self, **params) :
        template = lookup.get_template("utilisateur.html")
        reqDeleteUser(params)
        self.charHTML_dropMenu_user = constructHTML_dropMenu("numCreateur", reqDropMenu_User())
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("utilisateur"))
        self.idMaxUser = executeRequest(_requetes["getMaxIDUser"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateUser=self.charHTML_updateUser, idMaxUser=self.idMaxUser
            )
    # Mise à jour d'un utilisateur
    @cherrypy.expose
    def initiateUpdateUser(self, **params) :
        template = lookup.get_template("utilisateur.html")
        self.previousData = getParamsToUpdate("utilisateur", params)
        self.charHTML_updateUser = constructHTML_updateUser(self.previousData)
        self.idMaxUser = executeRequest(_requetes["getMaxIDUser"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateUser=self.charHTML_updateUser, idMaxUser=self.idMaxUser
            )
    @cherrypy.expose
    def userGetUpdate(self, **params) :
        template = lookup.get_template("utilisateur.html")
        updatingUserData(self.previousData, params)
        self.charHTML_updateUser = ""
        self.previousData = ""
        self.charHTML_tabAffichage = constructHTML_tabAffichage(reqAllInTable("utilisateur"))
        self.idMaxUser = executeRequest(_requetes["getMaxIDUser"])[1][0][0]
        return template.render(
            charHTML_tabAffichage=self.charHTML_tabAffichage, charHTML_updateUser=self.charHTML_updateUser, idMaxUser=self.idMaxUser
            )
        
    ############################################################################################################
    ############################################################################################################
    ### --- Page du sondage --- ###
    @cherrypy.expose
    def sondage(self) :
        template = lookup.get_template("sondage.html")
        self.charHTML_actualSondage = constructHTML_actualSondage()
        self.charHTML_pastSondage = constructHTML_pastSondage()
        self.charHTML_DropMenusMajSond = constructHTML_MajSondage()
        return template.render(
            charHTML_actualSondage=self.charHTML_actualSondage, charHTML_pastSondage=self.charHTML_pastSondage,
            charHTML_DropMenusMajSond=self.charHTML_DropMenusMajSond)
    @cherrypy.expose
    def voter1(self) :
        template = lookup.get_template("sondage.html")
        AVoter("1")
        self.charHTML_actualSondage = constructHTML_actualSondage()
        return template.render(
            charHTML_actualSondage=self.charHTML_actualSondage, charHTML_pastSondage=self.charHTML_pastSondage,
            charHTML_DropMenusMajSond=self.charHTML_DropMenusMajSond)    
    @cherrypy.expose
    def voter2(self) :
        template = lookup.get_template("sondage.html")
        AVoter("2")
        self.charHTML_actualSondage = constructHTML_actualSondage()
        return template.render(
            charHTML_actualSondage=self.charHTML_actualSondage, charHTML_pastSondage=self.charHTML_pastSondage,
            charHTML_DropMenusMajSond=self.charHTML_DropMenusMajSond)
    @cherrypy.expose
    def voter3(self) :
        template = lookup.get_template("sondage.html")
        AVoter("3")
        self.charHTML_actualSondage = constructHTML_actualSondage()
        return template.render(
            charHTML_actualSondage=self.charHTML_actualSondage, charHTML_pastSondage=self.charHTML_pastSondage,
            charHTML_DropMenusMajSond=self.charHTML_DropMenusMajSond)
    @cherrypy.expose
    def majSondage(self, **params) :
        template = lookup.get_template("sondage.html")
        insertDone = insertNewSondage(params)
        self.charHTML_actualSondage = constructHTML_actualSondage()
        if insertDone :
            self.charHTML_pastSondage = constructHTML_pastSondage()
        return template.render(
            charHTML_actualSondage=self.charHTML_actualSondage, charHTML_pastSondage=self.charHTML_pastSondage,
            charHTML_DropMenusMajSond=self.charHTML_DropMenusMajSond)


if __name__ == '__main__':
    print(f"\n    La racine du site est :\n\t{os.path.abspath(os.getcwd())}\n\tCet emplacement contient les fichiers et répertoires suivants : {os.listdir()}\n")
    initBase()
    cherrypy.quickstart(InterfaceWebMusique(), '/', 'configWeb.txt')