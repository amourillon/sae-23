-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 07 juin 2023 à 00:00
-- Version du serveur : 8.0.31
-- Version de PHP : 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `musique`
--

-- --------------------------------------------------------

--
-- Structure de la table `album`
--

DROP TABLE IF EXISTS `album`;
CREATE TABLE IF NOT EXISTS `album` (
  `numAlbum` int NOT NULL AUTO_INCREMENT,
  `nomAlbum` varchar(75) NOT NULL,
  `classification` enum('Album','EP','Single') NOT NULL,
  `dateSortie` date NOT NULL,
  `numArtiste` int DEFAULT '1',
  PRIMARY KEY (`numAlbum`),
  KEY `posseder` (`numArtiste`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `album`
--

INSERT INTO `album` (`numAlbum`, `nomAlbum`, `classification`, `dateSortie`, `numArtiste`) VALUES
(1, '-inconnu-', 'Single', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `artiste`
--

DROP TABLE IF EXISTS `artiste`;
CREATE TABLE IF NOT EXISTS `artiste` (
  `numArtiste` int NOT NULL AUTO_INCREMENT,
  `nomArtiste` varchar(75) NOT NULL,
  `descripArt` varchar(250) NOT NULL,
  PRIMARY KEY (`numArtiste`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `artiste`
--

INSERT INTO `artiste` (`numArtiste`, `nomArtiste`, `descripArt`) VALUES
(1, '-inconnu-', '-À utiliser quand vous ne connaissez pas l\'artiste-');

-- --------------------------------------------------------

--
-- Structure de la table `playlist`
--

DROP TABLE IF EXISTS `playlist`;
CREATE TABLE IF NOT EXISTS `playlist` (
  `numPlaylist` int NOT NULL AUTO_INCREMENT,
  `nomPlaylist` varchar(75) NOT NULL,
  `theme` varchar(250) NOT NULL,
  `dateCreation` date NOT NULL DEFAULT (curdate()),
  `numCreateur` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`numPlaylist`),
  KEY `constituer` (`numCreateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `rassembler`
--

DROP TABLE IF EXISTS `rassembler`;
CREATE TABLE IF NOT EXISTS `rassembler` (
  `numInteraTiPl` int NOT NULL AUTO_INCREMENT,
  `numTitre` int NOT NULL,
  `numPlaylist` int NOT NULL,
  PRIMARY KEY (`numTitre`,`numPlaylist`),
  UNIQUE KEY `numInteraTiPl` (`numInteraTiPl`),
  KEY `rassemblerSideP` (`numPlaylist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `sondage`
--

DROP TABLE IF EXISTS `sondage`;
CREATE TABLE IF NOT EXISTS `sondage` (
  `numSondage` int NOT NULL AUTO_INCREMENT,
  `numPlaylist_1` int NOT NULL,
  `nbVotePl_1` int DEFAULT '0',
  `numPlaylist_2` int NOT NULL,
  `nbVotePl_2` int DEFAULT '0',
  `numPlaylist_3` int NOT NULL,
  `nbVotePl_3` int DEFAULT '0',
  PRIMARY KEY (`numPlaylist_1`,`numPlaylist_2`,`numPlaylist_3`),
  UNIQUE KEY `numSondage` (`numSondage`),
  KEY `sondagePl_2` (`numPlaylist_2`),
  KEY `sondagePl_3` (`numPlaylist_3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `titre`
--

DROP TABLE IF EXISTS `titre`;
CREATE TABLE IF NOT EXISTS `titre` (
  `numTitre` int NOT NULL AUTO_INCREMENT,
  `nomTitre` varchar(75) NOT NULL,
  `duree` time NOT NULL,
  `numAlbum` int DEFAULT '1',
  `numArtiste` int DEFAULT '1',
  PRIMARY KEY (`numTitre`),
  KEY `grouper` (`numAlbum`),
  KEY `composer` (`numArtiste`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `numUtil` int NOT NULL AUTO_INCREMENT,
  `pseudonyme` varchar(75) NOT NULL,
  `descripUtil` varchar(250) NOT NULL,
  PRIMARY KEY (`numUtil`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`numUtil`, `pseudonyme`, `descripUtil`) VALUES
(1, 'Invité', '-utilisateur anonyme-');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `posseder` FOREIGN KEY (`numArtiste`) REFERENCES `artiste` (`numArtiste`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `playlist`
--
ALTER TABLE `playlist`
  ADD CONSTRAINT `constituer` FOREIGN KEY (`numCreateur`) REFERENCES `utilisateur` (`numUtil`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `rassembler`
--
ALTER TABLE `rassembler`
  ADD CONSTRAINT `rassemblerSideP` FOREIGN KEY (`numPlaylist`) REFERENCES `playlist` (`numPlaylist`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rassemblerSideT` FOREIGN KEY (`numTitre`) REFERENCES `titre` (`numTitre`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sondage`
--
ALTER TABLE `sondage`
  ADD CONSTRAINT `sondagePl_1` FOREIGN KEY (`numPlaylist_1`) REFERENCES `playlist` (`numPlaylist`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sondagePl_2` FOREIGN KEY (`numPlaylist_2`) REFERENCES `playlist` (`numPlaylist`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sondagePl_3` FOREIGN KEY (`numPlaylist_3`) REFERENCES `playlist` (`numPlaylist`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `titre`
--
ALTER TABLE `titre`
  ADD CONSTRAINT `composer` FOREIGN KEY (`numArtiste`) REFERENCES `artiste` (`numArtiste`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `grouper` FOREIGN KEY (`numAlbum`) REFERENCES `album` (`numAlbum`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
