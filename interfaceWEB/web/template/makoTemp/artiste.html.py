# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1686014072.860938
_enable_loop = True
_template_filename = 'web/template/artiste.html'
_template_uri = 'artiste.html'
_source_encoding = 'utf-8'
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'template.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        idMaxArtiste = context.get('idMaxArtiste', UNDEFINED)
        charHTML_tabAffichage = context.get('charHTML_tabAffichage', UNDEFINED)
        charHTML_updateArtiste = context.get('charHTML_updateArtiste', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n<div class="w3-content w3-justify">\r\n    <!-- Insertion d\'artistes -->\r\n    <section>\r\n        <form action="insertIntoArtiste">\r\n            <h3>Ajouter un nouvel artiste ou un nouveau groupe : </h3>\r\n            <h4 class="leftMarge">Nom de l\'artiste ou du groupe* :</h4>\r\n            <input type="search" name="artisteName" placeholder="Tapez un nom d\'artiste ou de groupe ... " class="insertPseudoBar" maxlength="75" required>\r\n            <br>\r\n            <div class="leftMarge decalageHaut">\r\n                <label class="labelDescri lowLineHeight" for="artisteDescri">Description de l\'artiste ou du groupe :<span class="fontSize16"> décrivez l\'artiste ou le \r\n                    groupe si vous le connaissez un peu (genre, origine, inspiration, etc)</span></label>\r\n                <textarea id="artisteDescri" name="artisteDescri" rows="5" cols="92" maxlength="250"></textarea>\r\n            </div>\r\n            <button type="submit" class="submitButton submitButtonHover standardButton">Ajouter</button>\r\n        </form>\r\n    </section>\r\n    <hr>\r\n    <!-- Suppression d\'albums -->\r\n    <section>\r\n        <h3>Supprimer un artiste ou un groupe :<span class="fontSize16"> supprimez en renseignant l\'ID <u>OU</u> le nom de l\'artiste ou du groupe</span></h3>\r\n        <form action="deleteArtiste" >\r\n            <div class="suppression leftMarge decalageBas">\r\n                <div class="displayRow supprID">\r\n                    <h4 class="textNoJustify" id="littleBox">Par son ID :</h4>\r\n                    <input class="insertPseudoBar" id="deleteByID" type="number" min="1" max="')
        __M_writer(str(idMaxArtiste))
        __M_writer('" name="artisteIdDelete" placeholder="ID ... " >\r\n                </div>\r\n                <div class="displayRow supprName">\r\n                    <h4 class="textNoJustify">Par son nom :</h4>\r\n                    <input class="insertPseudoBar" id="deleteByName" type="search" name="artisteNameDelete" placeholder="Nom ... "  maxlength="75">\r\n                </div>\r\n            </div>\r\n            <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Supprimer</button>\r\n        </form>\r\n    </section>\r\n    <hr>\r\n    <!-- Modification d\'artiste par ID -->\r\n    <section>\r\n        <div>\r\n            <form action="initiateUpdateArtiste">\r\n                <h3 class="lowLineHeight">Modifier un artiste ou un groupe :<span class="fontSize16 lowLineHeight"> accédez à la modification \r\n                    des informations d\'un artiste ou d\'un groupe en renseignant son ID</span></h3>\r\n                <div class="displayRow leftMarge">\r\n                    <h4>ID de l\'artiste ou du groupe à modifier : </h4>\r\n                    <div class="displayRow">\r\n                        <input type="number" min="1" max="')
        __M_writer(str(idMaxArtiste))
        __M_writer('" name="searchArtiste" placeholder="Tapez un ID ... " class="searchBar updateSearchBar" required>\r\n                        <input type="submit" value="" class="searchButton updateSearchBar"/>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </div>\r\n        <div>\r\n            ')
        __M_writer(str(charHTML_updateArtiste))
        __M_writer('\r\n        </div>\r\n    </section>\r\n    <hr>\r\n    <!-- Sélection -->\r\n    <section>\r\n        <div>\r\n            <form action="barSearchArtiste">\r\n                <h3>Rechercher par nom d\'artiste ou de groupe :<span class="fontSize16"> cherchez un artiste ou un groupe en renseignant son nom</span></h3>\r\n                <input type="search" name="searchArtiste" placeholder="Tapez un nom d\'artiste ou de groupe ... " class="searchBar"  maxlength="75" required>\r\n                <input type="submit" value="" class="searchButton"/>\r\n            </form>\r\n        </div>\r\n        <div>\r\n            <h3 class="marginBottomMedium">Trier l\'affichage :<span class="fontSize16"> triez la façon dont les artistes et les groupes sont affichés</span></h3>\r\n            <form action="sortSearchArtiste">\r\n                <div class="leftMarge">\r\n                    <input type="radio" class="sortRadio radio1" id="idArtiste" name="sortSelection" value="numArtiste" checked>\r\n                    <label for="idArtiste">Par ID</label>\r\n\r\n                    <input type="radio" class="sortRadio radio2" id="nameArtiste" name="sortSelection" value="nomArtiste">\r\n                    <label for="nameArtiste">Par artiste</label>\r\n                    \r\n                    <label class="sortBox">Inverser l\'ordre \r\n                        <input type="checkbox" id="ordreInverse" name="ordreInverse" value=1>\r\n                        <span class="sortCheckmark"></span>\r\n                    </label>\r\n                </div>\r\n                <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Trier</button>\r\n            </form>\r\n        </div>\r\n    </section>\r\n    <hr>\r\n    <!-- Affichage -->\r\n    <section>\r\n        <h3><u>Affichage des artistes/groupes :</u></h3>\r\n        ')
        __M_writer(str(charHTML_tabAffichage))
        __M_writer('\r\n    </section>\r\n</div>')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "web/template/artiste.html", "uri": "artiste.html", "source_encoding": "utf-8", "line_map": {"27": 0, "35": 1, "36": 2, "37": 27, "38": 27, "39": 47, "40": 47, "41": 54, "42": 54, "43": 90, "44": 90, "50": 44}}
__M_END_METADATA
"""
