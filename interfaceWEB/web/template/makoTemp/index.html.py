# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1686081224.6315415
_enable_loop = True
_template_filename = 'web/template/index.html'
_template_uri = 'index.html'
_source_encoding = 'utf-8'
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'template.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n<div class="w3-content w3-justify">\r\n    <!-- Présentation rapide -->\r\n    <section>\r\n        <h1 class="centeredText"> Bienvenue sur EasyTune </h1>\r\n        <p class="tabulation fontSize20">\r\n                Bienvenue sur <a class="customizedPink">EasyTune</a>, votre interface de gestion de votre base de données musicale. Ici, vous \r\n            pourrez <a class="customizedGreen">ajouter, modifiez, supprimez et affichez</a> les titres, artistes et albums disponibles\r\n            dans la base de données. Vous pourrez également créer et gérer <a class="customizedGreen">vos propres playlists</a>, et \r\n            explorer des nouveaux styles musicaux en regardant les playlists des autres utilisateurs. \r\n        </p>\r\n        <p class="tabulation fontSize20">\r\n                De plus, notre administrateur met régulière à jour le sondage de la page <a class="customizedPink">"Sondage".</a> Là-bas, \r\n            vous pouvez <a class="customizedGreen">voter</a> pour une des trois playlists présentées et ainsi élire la meilleure de \r\n            celles-ci. Les playlists sont tirées au sort parmi toutes celles créer par les utilisateurs. Vos playlists seront-elles élues les \r\n            <a class="customizedPink fontBold">meilleures</a> ?\r\n        </p>\r\n        <hr>\r\n        <h2 class=""> Conseils d\'utilisations - Lisez moi :]</h2>\r\n        <p class="tabulation fontSize20">\r\n                Afin d\'avoir une expérience des plus agréables au cours de l\'utilisation d\'<a class="customizedPink">EasyTune</a>, l\'administrateur\r\n            du site vous apporte quelques conseils et règles. Respecter ces quelques points, et vous n\'expérimenterez <a class="customizedGreen">aucun\r\n            bug</a> ou autres désagréments.\r\n        </p>\r\n        <ul class="fontSize20" style="margin-bottom: 3%;">\r\n            <li>Les sections comportant la mention <a class="customizedPink">"ADMIN"</a> ne sont pas destinées aux utilisateurs. Veuillez vous référer à l\'administrateur pour \r\n                plus de détails\r\n            </li>\r\n            <li>\r\n                Les différents menus déroulants sont triés par <a class="customizedGreen">ordre alphabétique</a>\r\n            </li>\r\n            <li>\r\n                Lors d\'une insertion de texte, évitez les guillements (remplacez les par 1 ou 2 apostrophe(s))\r\n            </li>\r\n            <li>\r\n                Si un champ comportant le caractère "<a class="customizedGreen">*</a>", il doit être remplie obligatoire pour effectuer quelconque action.\r\n            </li>\r\n            <li>Il existe quelques suppressions <a class="customizedGreen">"dangereuses"</a>. Ces suppressions engendreront la majeure partie du temps la suppression de données\r\n                liées à la donnée supprimée initialement. Par exemple, supprimer un artiste supprime également les albums et les titres qui lui\r\n                sont liés. Cette règle s\'applique aussi aux playlists liées à des utilisateurs, ainsi qu\'aux titres liés à des albums.<br>\r\n                <a class="customizedPink">Soyez donc vigilant !</a>\r\n            </li>\r\n            <li>Merci de conservé les données d\'ID n° 1 des pages "Albums", "Artiste" et "Utilisateurs". Ces données servent notamment aux titres dont \r\n                l\'album est <a class="customizedGreen">inconnu</a>, mais également aux titres et aux albums dont l\'artiste est <a class="customizedGreen">\r\n                inconnu</a>, et enfin aux utilisateurs souhaitant restés <a class="customizedGreen">anonymes</a>. \r\n            </li>\r\n            <li>\r\n                Il est possible qu\'en de rares occasions, il soit possible de renseigner des <a class="customizedPink">IDs qui n\'existent pas.</a>Il est\r\n                donc fortement déconseillé évidemment de rentrer une ID incorrecte dans ces cas-là.\r\n            </li>\r\n            <li>\r\n                Lisez les consignes : l\'utilité de chaque champs est expliqué au-dessus de celui-ci. Prendre le temps de lire ces descriptions vous évitera\r\n                des erreurs\r\n            </li>\r\n            <li>\r\n                Il est conseillé d\'utiliser <a class="customizedPink">EasyTune</a> sur ordinateur, en plein écran, pour un maximum de confort (même si \r\n                l\'interface fonctionne sous d\'autres formats).\r\n            </li>\r\n            <li>\r\n                Pour finir, et de façon plus globale,  <a class="customizedPink">partagez</a> et <a class="customizedGreen fontBold">amusez-vous</a> ! Tous \r\n                les utilisateurs de EasyTune ont un point commun : l\'amour de la musique. <a class="customizedGreen"></a>Soyez ouvert et respectueux</a>,vous\r\n                trouverez sûrement une pépite sonore se cachant dans notre base de données :D\r\n            </li>\r\n        </ul>\r\n    </section>\r\n    <hr>\r\n    <!-- Image d\'illustration -->\r\n    <section>\r\n        <img class="imageScale" src="/static/media/images/IllustrationAccueil.png" id="illustrationAccueil">\r\n    </section>\r\n    <hr>\r\n    <!-- Importation / Exportation CSVs / Reinit Tables / Reinit DB -->\r\n    <section>\r\n        <h3 style="margin-bottom: 1px;">ADMIN - Utilitaires de l\'interface web : </h3>\r\n        <div id="styleImportExport" class="textToLeft">\r\n            <form action="importCSV" id="importCSV">\r\n                <h4>Importer les données<br>sauvegardées :</h4>\r\n                <button type="submit" class="submitButton submitButtonHover standardButton">Importer</button>\r\n            </form>\r\n            <form action="exportCSV">\r\n                <h4 class="toLittleH4">Exporter les données : </h4>\r\n                <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Exporter</button>\r\n            </form>\r\n            <form action="reinitialisationTables">\r\n                <h4 class="toLittleH4">Réinitialiser les tables : </h4>\r\n                <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Réinitialiser</button>\r\n            </form>\r\n            <form action="reinitialisationDataBase">\r\n                <h4>Recréer la base<br>de données : </h4>\r\n                <button type="submit" class="submitButton submitButtonHover standardButton">Recréer</button>\r\n            </form>\r\n        </div>\r\n    </section>\r\n</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "web/template/index.html", "uri": "index.html", "source_encoding": "utf-8", "line_map": {"27": 0, "32": 1, "33": 2, "39": 33}}
__M_END_METADATA
"""
