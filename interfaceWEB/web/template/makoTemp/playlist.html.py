# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1686014073.7884505
_enable_loop = True
_template_filename = 'web/template/playlist.html'
_template_uri = 'playlist.html'
_source_encoding = 'utf-8'
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'template.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        charHTML_dropMenu_tit = context.get('charHTML_dropMenu_tit', UNDEFINED)
        charHTML_dropMenu_play = context.get('charHTML_dropMenu_play', UNDEFINED)
        charHTML_updatePlaylist = context.get('charHTML_updatePlaylist', UNDEFINED)
        messErr = context.get('messErr', UNDEFINED)
        charHTML_dropMenu_user = context.get('charHTML_dropMenu_user', UNDEFINED)
        idMaxPlaylist = context.get('idMaxPlaylist', UNDEFINED)
        charHTML_tabAffichage = context.get('charHTML_tabAffichage', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n<div class="w3-content w3-justify">\r\n    <section class="displayRow">\r\n        <!-- Insertion de playlist -->\r\n        <div class="affPlaylists">\r\n            <form action="insertIntoPlaylist">\r\n                <h3>Ajouter une nouvelle playlist : </h3>\r\n                <div class="decalageBas">\r\n                    <h4 class="leftMarge">Nom de la nouvelle playlist* :</h4>\r\n                    <input type="search" id="playlistName" name="playlistName" placeholder="Tapez un nom de playlist ... " class="insertPseudoBar" maxlength="75" required>\r\n                    <br>\r\n                    <div>\r\n                        <h4 class="leftMarge">Indiquez l\'utilisateur qui crée cette playlist :</h4>\r\n                        <div class="leftMarge dropMenuAlbArtUser">\r\n                            ')
        __M_writer(str(charHTML_dropMenu_user))
        __M_writer('\r\n                        </div>\r\n                    </div>\r\n                    <div class="leftMarge lowLineHeight">\r\n                        <label class="labelDescri" for="playlistTheme">Décrivez la nouvelle playlist :<span class="fontSize16"> décrivez le thème de cette \r\n                            playlist,<br>ses inspirations, etc.</span></label>\r\n                        <textarea id="playlistTheme" name="playlistTheme" rows="5" cols="63" maxlength="250"></textarea>\r\n                    </div>\r\n                </div>\r\n                <button type="submit" class="submitButton submitButtonHover standardButtonPl">Ajouter</button>\r\n            </form>\r\n        </div>\r\n        <!-- Insertion de titre dans les playlists -->\r\n        <div class="affContenuPlaylist needToBeCenteredRow" style="padding-bottom: 2%;">\r\n            <div>\r\n                <h3>Associer un titre à une playlist : </h3>\r\n                <form action="insertTitreInPlaylist">\r\n                    <div class="displayCol leftMarge decalageBas">\r\n                        <div>\r\n                            <h4>Indiquez un titre à<br>associer :</h4>\r\n                            <div class="thickDropMenuPlus">\r\n                                ')
        __M_writer(str(charHTML_dropMenu_tit))
        __M_writer('\r\n                            </div>\r\n                        </div>\r\n                        <div>\r\n                            <h4 class="textNoJustify">Indiquez la playlist où ajouter ce titre :</h4>\r\n                            <div class="thickDropMenuPlus">\r\n                                ')
        __M_writer(str(charHTML_dropMenu_play))
        __M_writer('\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <h5 class="leftMarge textNoJustify">')
        __M_writer(str(messErr))
        __M_writer('</h5>\r\n                    <button type="submit" class="submitButton buttonHoverCustomGreen standardButtonPl">Ajouter</button>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <hr>\r\n    <!-- Suppression de playlist -->\r\n    <section class="displayRow">\r\n        <div class="affContenuPlaylistReverse">\r\n            <h3 class="textNoJustify lowLineHeight">Supprimer une playlist :<span class="fontSize16"> supprimez en renseignant l\'ID <u>OU</u> le nom de la playlist</span></h3>\r\n            <form action="deletePlaylist" >\r\n                <div class="displayCol leftMarge decalageBas">\r\n                    <h4 class="textNoJustify">Par son ID :</h4>\r\n                    <input class="insertPseudoBar leftMarge" min="1" type="number" max="')
        __M_writer(str(idMaxPlaylist))
        __M_writer('" name="playlistIdDelete" placeholder="ID ... " >\r\n                \r\n                    <h4 class="textNoJustify">Par son nom :</h4>\r\n                    <input class="insertPseudoBar leftMarge decalageBas" type="search" name="playlistNameDelete" placeholder="Nom ... " maxlength="75">\r\n                </div>\r\n                <button type="submit" class="submitButton buttonHoverCustomGreen standardButtonPl" style="margin-top: 4%;">Supprimer</button>\r\n            </form>\r\n        </div>\r\n        <!-- Suppression de titre dans les playlists -->\r\n        <div class="affPlaylistsReverse needToBeCenteredRow">\r\n            <div>\r\n                <h3 class="lowLineHeight">Supprimer un titre d\'une playlist :<span class="fontSize16"> sélectionnez la playlist comportant le titre à supprimer,\r\n                    puis renseignez l\'ID <u>OU</u> le nom du titre à éliminer</span></h3>\r\n                <form action="deleteIntoPlaylist" >\r\n                    <div class="displayCol leftMarge decalageBas">\r\n                        <div class="decalageBas">\r\n                            <h4 class="textNoJustify">Sélectionner la playlist : </h4>\r\n                            <div class="leftMarge thickDropMenuMoins">\r\n                                ')
        __M_writer(str(charHTML_dropMenu_play))
        __M_writer('\r\n                            </div>\r\n                        </div>\r\n                        <div class="displayRow">\r\n                            <h4 id="supprInPlayID">Par son ID :</h4>\r\n                            <input class="insertPseudoBar" id="deleteByIDPlay" min="1" type="number" max="')
        __M_writer(str(idMaxPlaylist))
        __M_writer('" name="intoPlaylistIdDelete" placeholder="ID ... " >\r\n                        </div>\r\n                        <div class="displayRow">\r\n                            <h4 class="textNoJustify" id="supprInPlayName">Par son nom :</h4>\r\n                            <input class="insertPseudoBar" id="deleteByNamePlay" type="search" name="intoPlaylistNameDelete" placeholder="Nom ... " maxlength="75">\r\n                        </div>\r\n                    </div>\r\n                    <button type="submit" class="submitButton buttonHoverCustomGreen standardButtonPl">Supprimer</button>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <hr>\r\n    <!-- Modification de playlist par ID -->\r\n    <section>\r\n        <div>\r\n            <form action="initiateUpdatePlaylist">\r\n                <h3 class="lowLineHeight">Modifier une playlist :<span class="fontSize16 lowLineHeight"> accédez à la modification \r\n                    des informations d\'une playlist en renseignant son ID</span></h3>\r\n                <div class="displayRow leftMarge">\r\n                    <h4>ID de la playlist à modifier* : </h4>\r\n                    <div class="displayRow">\r\n                        <input type="number" min="1" max="')
        __M_writer(str(idMaxPlaylist))
        __M_writer('" name="searchPlaylist" placeholder="Tapez un ID ... " class="searchBar updateSearchBar" required/>\r\n                        <input type="submit" value="" class="searchButton updateSearchBar"/>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </div>\r\n        <div>\r\n            ')
        __M_writer(str(charHTML_updatePlaylist))
        __M_writer('\r\n        </div>\r\n    </section>\r\n    <hr>\r\n    <!-- Sélection -->\r\n    <div class="displayRow">\r\n        <div class="affPlaylists">\r\n            <div>\r\n                <form action="barSearchPlaylist">\r\n                    <h3 class="lowLineHeight">Rechercher par nom de playlist* :<span class="fontSize16 lowLineHeight"> cherchez une playlist en \r\n                        <br> renseignant son nom</span></h3>\r\n                    <input type="search" name="searchPlaylist" placeholder="Tapez un nom de playlist ... " class="searchBar" maxlength="75" required>\r\n                    <input type="submit" value="" class="searchButton"/>\r\n                </form>\r\n            </div>\r\n            <div>\r\n                <h3 class="marginBottomMedium">Trier l\'affichage :<span class="fontSize16"> triez la façon dont les playlists sont affichés</span></h3>\r\n                <form action="sortSearchPlaylist">\r\n                    <div class="displayRow">\r\n                        <div class="leftMarge affParID">\r\n                            <input type="radio" class="sortRadio radio1 radiodeb" id="idPlaylist" name="sortSelection" value="numPlaylist" checked>\r\n                            <label for="idPlaylist">Par ID</label>\r\n                        </div>\r\n                        <div class="critereTriAffPlay">\r\n                            <input type="radio" class="sortRadio radio2" id="namePlaylist" name="sortSelection" value="nomPlaylist">\r\n                            <label for="namePlaylist">Par nom de playlist</label>\r\n                        </div>\r\n                        <div class="critereTriAffPlay">\r\n                            <input type="radio" class="sortRadio radio2 radiodeb" id="userPlaylist" name="sortSelection" value="pseudonyme">\r\n                            <label for="userPlaylist">Par créateur</label>\r\n                        </div>\r\n                        <div class="critereTriAffPlay">\r\n                            <input type="radio" class="sortRadio radio2" id="datePlaylist" name="sortSelection" value="dateCreation">\r\n                            <label for="datePlaylist">Par date de création</label>\r\n                        </div>\r\n                    </div>\r\n                    <div>\r\n                        <label id="inverseOrdrePlay" class="sortBox">Inverser l\'ordre \r\n                            <input type="checkbox" id="ordreInverse" name="ordreInverse" value=1>\r\n                            <span class="sortCheckmark"></span>\r\n                        </label>\r\n                    </div>\r\n                    <button type="submit" class="submitButton buttonHoverCustomGreen standardButtonPl">Trier</button>\r\n                </form>\r\n            </div>\r\n        </div>\r\n        <!-- Affichage de contenu de playlist -->\r\n        <div class="affContenuPlaylist needToBeCenteredRow" style="padding-bottom: 5%;">\r\n            <form action="affTitreInPlaylist">\r\n                <h3 class="textNoJustify">Afficher les titres d\'une playlist : </h3>\r\n                <div class="leftMarge decalageBas" id="dropContenuPlaylist">\r\n                    ')
        __M_writer(str(charHTML_dropMenu_play))
        __M_writer('\r\n                </div>\r\n                <!-- <br><br><br><br style="margin-top:16px"> -->\r\n                <button type="submit" class="submitButton buttonHoverCustomGreen submitContenuPlaylist standardButtonPl">Afficher</button>\r\n            </form>\r\n        </div>\r\n    </div>\r\n    <hr>\r\n    <!-- Affichage -->\r\n    <section>\r\n        <h3><u>Affichage des playlists, ou du contenu de l\'une d\'entre elles :</u></h3>\r\n        ')
        __M_writer(str(charHTML_tabAffichage))
        __M_writer('\r\n    </section>\r\n</div>')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "web/template/playlist.html", "uri": "playlist.html", "source_encoding": "utf-8", "line_map": {"27": 0, "39": 1, "40": 2, "41": 16, "42": 16, "43": 37, "44": 37, "45": 43, "46": 43, "47": 47, "48": 47, "49": 61, "50": 61, "51": 79, "52": 79, "53": 84, "54": 84, "55": 106, "56": 106, "57": 113, "58": 113, "59": 164, "60": 164, "61": 175, "62": 175, "68": 62}}
__M_END_METADATA
"""
