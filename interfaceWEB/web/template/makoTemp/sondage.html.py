# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1686010241.5207438
_enable_loop = True
_template_filename = 'web/template/sondage.html'
_template_uri = 'sondage.html'
_source_encoding = 'utf-8'
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'template.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        charHTML_actualSondage = context.get('charHTML_actualSondage', UNDEFINED)
        charHTML_DropMenusMajSond = context.get('charHTML_DropMenusMajSond', UNDEFINED)
        charHTML_pastSondage = context.get('charHTML_pastSondage', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n<div class="w3-content w3-justify">\r\n    <!-- Affichage sondage -->\r\n    <div class="displayCol">\r\n        ')
        __M_writer(str(charHTML_actualSondage))
        __M_writer('\r\n    </div>\r\n    <br>\r\n    <hr>\r\n    <!-- Mise à jour du sondage -->\r\n    <div>\r\n        <div>\r\n            <h2>ADMIN - Mise à jour du sondage : </h2>\r\n            <h4 class="leftMarge">Choississez le nouveau sondage à mettre en ligne :</h4>\r\n        </div>\r\n        <form action="majSondage">\r\n            <div class="displayRow dispDropMenu">\r\n                ')
        __M_writer(str(charHTML_DropMenusMajSond))
        __M_writer('\r\n            </div>\r\n            <div class="buttonSonder">\r\n                <button type="submit" class="submitButton submitButtonHover standardButton">Sonder</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <br>\r\n    <hr>\r\n    <!-- Affichage des anciens vainqueurs du sondage -->\r\n    <div>\r\n        <h2 class="vainqueurCentered">Vainqueurs des anciens sondages : </h2>\r\n        <div class="displayCol affVainqueurPasse">\r\n            ')
        __M_writer(str(charHTML_pastSondage))
        __M_writer('\r\n        </div>\r\n    </div>\r\n</div>')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "web/template/sondage.html", "uri": "sondage.html", "source_encoding": "utf-8", "line_map": {"27": 0, "35": 1, "36": 2, "37": 6, "38": 6, "39": 18, "40": 18, "41": 31, "42": 31, "48": 42}}
__M_END_METADATA
"""
