# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1686014074.2664273
_enable_loop = True
_template_filename = 'web/template/utilisateur.html'
_template_uri = 'utilisateur.html'
_source_encoding = 'utf-8'
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'template.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        charHTML_tabAffichage = context.get('charHTML_tabAffichage', UNDEFINED)
        idMaxUser = context.get('idMaxUser', UNDEFINED)
        charHTML_updateUser = context.get('charHTML_updateUser', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n<div class="w3-content w3-justify">\r\n    <!-- Insertion d\'utilisateurs -->\r\n    <section>\r\n        <form action="insertIntoUser">\r\n            <h3>Ajouter un nouvel utilisateur : </h3>\r\n            <h4 class="leftMarge">Pseudonyme du nouvel utilisateur* :</h4>\r\n            <input type="search" name="userPseudo" placeholder="Tapez un pseudonyme ... " class="insertPseudoBar" maxlength="75" required>\r\n            <br>\r\n            <div class="leftMarge decalageHaut">\r\n                <label class="labelDescri lowLineHeight" for="userDescri">Description de l\'utilisateur :<span class="fontSize16"> décrivez l\'utilisateur\r\n                    (vous peut-être), si vous le souhaitez (styles préférés ou détestés, caractéristiques, etc)</span></label>\r\n                <textarea id="userDescri" name="userDescri" rows="5" cols="92" maxlength="250"></textarea>\r\n            </div>\r\n            <button type="submit" class="submitButton submitButtonHover standardButton">Ajouter</button>\r\n        </form>\r\n    </section>\r\n    <hr>\r\n    <!-- Suppression d\'utilisateurs -->\r\n    <section>\r\n        <h3>Supprimer un utilisateur :<span class="fontSize16"> supprimez en renseignant l\'ID <u>OU</u> le pseudonyme de l\'utilisateur</span></h3>\r\n        <form action="deleteUser" >\r\n            <div class="suppression leftMarge decalageBas">\r\n                <div class="displayRow supprID">\r\n                    <h4 class="textNoJustify" id="littleBox">Par son ID :</h4>\r\n                    <input class="insertPseudoBar" id="deleteByID" type="number" min="1" max="')
        __M_writer(str(idMaxUser))
        __M_writer('" name="userIdDelete" placeholder="ID ... " >\r\n                </div>\r\n                <div class="displayRow supprName">\r\n                    <h4 class="textNoJustify">Par son pseudonyme :</h4>\r\n                    <input class="insertPseudoBar" id="deleteByName" type="search" name="userNameDelete" placeholder="Pseudonyme ... " maxlength="75">\r\n                </div>\r\n            </div>\r\n            <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Supprimer</button>\r\n        </form>\r\n    </section>\r\n    <hr>\r\n    <!-- Modification d\'utilisateur par ID -->\r\n    <section>\r\n        <div>\r\n            <form action="initiateUpdateUser">\r\n                <h3 class="lowLineHeight">Modifier un utilisateur :<span class="fontSize16 lowLineHeight"> accédez à la modification \r\n                    des informations d\'un utilisateur en renseignant son ID</span></h3>\r\n                <div class="displayRow leftMarge">\r\n                    <h4>ID de l\'utilisateur à modifier* : </h4>\r\n                    <div class="displayRow">\r\n                        <input type="number" min="1" max="')
        __M_writer(str(idMaxUser))
        __M_writer('" name="searchUser" placeholder="Tapez un ID ... " class="searchBar updateSearchBar" required>\r\n                        <input type="submit" value="" class="searchButton updateSearchBar"/>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </div>\r\n        <div>\r\n            ')
        __M_writer(str(charHTML_updateUser))
        __M_writer('\r\n        </div>\r\n    </section>\r\n    <hr>\r\n    <!-- Sélection -->\r\n    <section>\r\n        <div>\r\n            <form action="barSearchUser">\r\n                <h3>Rechercher par nom d\'utilisateur :<span class="fontSize16"> cherchez un utilisateur en renseignant son pseudonyme</span></h3>\r\n                <input type="search" name="searchUser" placeholder="Tapez un pseudonyme ... " class="searchBar" maxlength="75" required>\r\n                <input type="submit" value="" class="searchButton"/>\r\n            </form>\r\n        </div>\r\n        <div>\r\n            <h3 class="marginBottomMedium">Trier l\'affichage :<span class="fontSize16"> triez la façon dont les utilisateurs sont affichés</span></h3>\r\n            <form action="sortSearchUser">\r\n                <div class="leftMarge decalageBas">\r\n                    <input type="radio" class="sortRadio radio1" id="idUser" name="sortSelection" value="numUtil" checked>\r\n                    <label for="idUser">Par ID</label>\r\n\r\n                    <input type="radio" class="sortRadio radio2" id="nameUser" name="sortSelection" value="pseudonyme">\r\n                    <label for="nameUser">Par nom d\'utilisateur</label>\r\n                    \r\n                    <label class="sortBox">Inverser l\'ordre \r\n                        <input type="checkbox" id="ordreInverse" name="ordreInverse" value="1">\r\n                        <span class="sortCheckmark"></span>\r\n                    </label>\r\n                </div>\r\n                <button type="submit" class="submitButton buttonHoverCustomGreen standardButton">Trier</button>\r\n            </form>\r\n        </div>\r\n    </section>\r\n    <hr>\r\n\r\n    <!-- Affichage -->\r\n    <section>\r\n        <h3><u>Affichage des utilisateurs :</u></h3>\r\n        ')
        __M_writer(str(charHTML_tabAffichage))
        __M_writer('\r\n    </section>\r\n</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "web/template/utilisateur.html", "uri": "utilisateur.html", "source_encoding": "utf-8", "line_map": {"27": 0, "35": 1, "36": 2, "37": 27, "38": 27, "39": 47, "40": 47, "41": 54, "42": 54, "43": 91, "44": 91, "50": 44}}
__M_END_METADATA
"""
